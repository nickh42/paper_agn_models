#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  5 17:54:53 2017

@author: nh444
"""

import matplotlib.pyplot as plt
import numpy as np

snapnum = 25

outdir = "/home/nh444/Documents/paper/"
outfilename = "xray_flux_maps.pdf"
h_scale=0.679

indirs = ["/home/nh444/data/project1/maps/MDRInt/c384_MDRInt/",
          "/data/curie4/nh444/project1/maps/L40_512_MDRIntRadioEff/",
          #"/home/nh444/data/project1/maps/MDRInt/c256_MDRInt/",
          ]
filebases = ["xray_flux_map_grp0_1.5Mpc_proj_16ngb_05-10", "xray_flux_map_grp1_0.6Mpc_proj_16ngb_05-10"]
file_suffixes = ["_default", "_Rasia2012"] ## added to each filebase
labels = ["Fiducial cut", "Rasia+2012"] ## labels corresponding to file_suffixes

circrad = [1.0] ## list of radii (r500) of circles drawn at centre, set empty list to disable

nplots = len(indirs)
ncol = len(file_suffixes)

fig = plt.figure(figsize=(6*ncol,6*nplots)) ## allow ~6 inches per map
labelsize = 18
prune = True

from mpl_toolkits.axes_grid1 import ImageGrid
cbar_mode="single"
cbar_size="6%"

for dirnum, indir in enumerate(indirs):
    filebase = filebases[dirnum]
    icol = 0 # column index
    irow = 0
    minval = np.inf
    maxval = -np.inf
    imgs = []
    grid = ImageGrid(fig, 211+dirnum,
                 nrows_ncols=(1,ncol),
                 axes_pad=0,
                 share_all=True, ## uses smallest xlim/ylim for all maps
                 label_mode="L", ## labels only on left and bottom
                 cbar_location="right",
                 cbar_mode=cbar_mode,
                 cbar_size=cbar_size,
                 cbar_pad=0,
                 )
    print np.shape(grid)
    for j, file_suffix in enumerate(file_suffixes):
        print "irow, icol, j =", irow, icol, j
        dat = np.load(indir+filebase+file_suffix+".npz")
        if len(dat["mappars"]) > 10:
            cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,R500,M500 = dat["mappars"]
        elif len(dat["mappars"]) > 8:
            cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,R500,M500 = dat["mappars"]
            print "M500 = {:.1f}e13".format(M500 / 1e3 / h_scale)
            print "R500 = {:.1f}".format(R500 / h_scale)
            print "sidelength =", sidelength
            print 0.5*sidelength/1000.0/h_scale
        else:
            cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2 = dat["mappars"]
        maparr = dat["maparr"]
        npix = maparr.shape[0]
        assert maparr.shape[1] == npix, "map not square."
        maparr /= (sidelength / h / (1+z) / npix)**2
        maparr = np.log10(maparr)
        if np.min(maparr) < minval: minval = np.min(maparr)
        if np.max(maparr) > maxval: maxval = np.max(maparr)
        imgs.append(grid[j].imshow(maparr.transpose(), cmap=plt.get_cmap('viridis'),
                        extent=[-0.5*sidelength/1000.0/h_scale, 0.5*sidelength/1000.0/h_scale, -0.5*sidelength/1000.0/h_scale, +0.5*sidelength/1000.0/h_scale],
                        origin="lower"))#, norm=LogNorm(vmin=maparr.min(), vmax=maparr.max())))
        grid[j].set_xlabel("x [Mpc]")
        grid[j].set_ylabel("y [Mpc]")
        if len(circrad) > 0:
            for crad in circrad:
                circle = plt.Circle((0, 0), crad * R500 / 1000.0 / h_scale, fc='none', ec='white', ls='dotted')
                grid[j].add_artist(circle)

        if prune and icol>0:
                xticks = grid[j].get_xticklabels()
                xticks[1].set_visible(False)
                
        label = labels[j].replace("_","\_")
        grid[j].annotate(label, xy=(0.08,0.9), xycoords="axes fraction", color='white', weight='bold', fontsize=labelsize)
        cbar = grid.cbar_axes[j].colorbar(imgs[j])
        cbar.solids.set_edgecolor("face") ## to fix white lines in colorbar in PDF
        #grid.cbar_axes[j].set_label("X-ray flux")
        axis = grid.cbar_axes[j].axis[grid.cbar_axes[j].orientation]
                
        if icol+1 < ncol:
            icol += 1
        else:
            irow += 1
            icol = 0
    

    print "minval, maxval =", minval, maxval
    for img in imgs:
        img.set_clim(vmin=minval, vmax=maxval)
    cbar = grid[0].cax.colorbar(imgs[0])
    cbar.solids.set_edgecolor("face") ## to fix white lines in colorbar in PDF
    cax = grid.cbar_axes[0]
    axis = cax.axis[cax.orientation]
    axis.label.set_text("$\mathrm{log}_{10}$($S_{X}$ [erg s$^{-1}$ cm$^{-2}$ kpc$^{-2}$])")
plt.savefig(outdir+outfilename, bbox_inches='tight', dpi=300)
print "Plot saved to", outdir+outfilename