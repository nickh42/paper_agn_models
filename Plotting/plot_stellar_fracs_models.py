#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 17:17:16 2017

@author: nh444
"""
def main():
    # General settings
    obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
    delta = 500.0
    ref = "crit"
    use_fof = True ## Use halo file for M_delta and R_delta
    h_scale = 0.679
    masstab = False
              
    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = ["L40_512_NoDutyRadioWeak/",
               "L40_512_NoDutyRadioInt/",
               "L40_512_DutyRadioWeak/",
               "L40_512_MDRIntRadioEff/",
           ]
    labels = ["weak radio",
              "stronger radio",
              "quasar duty cycle",
              "fiducial"]


    from palettable.colorbrewer.sequential import YlGnBu_5 as pallette
    col = pallette.hex_colors[-4:]
    col = ['#a1dab4', '#59a5bb', '#386dac', '#253494']
    col = ['#BBD8C0', '#50BFD7', '#287AC1', '#192473']
    col = ['#95CEB5', '#50BFD7', '#287AC1', '#192473']
    col = ['#FF9819', '#B94400', '#6395EC', '#6154BE'] ## Hydrangea orange-blue colour scheme
    col = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396'] ## orange-blue - combination of Viget and Hydrangea, slightly altered
#    from palettable.colorbrewer.sequential import Blues_4 as pallette
#    from palettable.colorbrewer.sequential import GnBu_4 as pallette
#    from palettable.colorbrewer.sequential import Purples_4 as pallette
#    from palettable.cartocolors.sequential import BluYl_4 as pallette
#    from palettable.cartocolors.sequential import Teal_4 as pallette
#    col=pallette.hex_colors[-3:]+['#E7298A']
#    from palettable.cartocolors.qualitative import Prism_9 as palette
#    col = palette.hex_colors[1::2]
    
    ls = ['solid']*len(simdirs)
    
    datadir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
    outdir = "/home/nh444/Documents/paper/"
    snapnum, z = 25, 0.0
    snapnums = [snapnum]
    
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    #outdir = "/data/curie4/nh444/project1/stars/"
    
    zoomdirs = [] ## corresponding to each simdir
    zooms = []
    zooms2 = []
    
#==============================================================================
#     from stars.stellar_mass import stellar_mass
#     for simdir in simdirs:
#         mstar = stellar_mass(basedir, simdir, datadir, snapnums, obs_dir, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=False)
#         #mstar.get_mass_fracs(M500min=1e12)
#         mstar.get_stellar_masses(M500min=10**11.6, verbose=True)
#         #mstar.plot_mstarfrac_v_mass(for_paper=True) #requires get_mass_fracs
#         #mstar.show_satellite_mass_distribution(range(0,1), cumulative=True, fractional=True)
#     
#==============================================================================
    plot_mstarfrac_v_mass_comparison_components(datadir, simdirs, zoomdirs, zooms, snapnum, z, incl_ICL=True, verbose=True,
                                                outfilename="stellar_mass_fractions_models.pdf",
                                                outdir=outdir, zooms2=zooms2, scat_idx=[], plot_med=True, for_paper=True, obs_dir=obs_dir, labels=labels, col=col, ls=ls, h_scale=h_scale)

def plot_mstarfrac_v_mass_comparison_components(basedir, simdirs, zoomdirs, zooms,
                                                snapnum, redshift,zooms2=[],
                                                obs_dir="/data/vault/nh444/ObsData/mass_fracs/",
                                                incl_ICL=True, outfilename="default", outdir="/data/curie4/nh444/project1/stars/", for_paper=True,
                                                scat_idx=[], labels=None, col=None, ls=None,
                                                plot_med=True, h_scale=0.7, verbose=False):
    """
    Plots stellar mass fraction within r500 using the output of stellar_mass.get_stellar_masses()
    zooms2 is another list of zooms which are plotted with the same colours as zooms but with open rather than solid symbols
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py
    import os

    z = redshift
    fout_base = "stellar_masses_"
    if outfilename == "default":
        if for_paper: outfilename = "stellar_mass_fractions_paper.pdf"
        else: outfilename = "stellar_mass_fractions.pdf"
    basedir = os.path.normpath(basedir)+"/"

    if incl_ICL:
        gonz = np.genfromtxt(obs_dir+"Gonzalez_2013_stellar_gas_masses.csv", delimiter=",")
        krav = np.genfromtxt(obs_dir+"kravtsov_stellar_masses2.csv", delimiter=",")
        sand = np.genfromtxt(obs_dir+"Sanderson_2013_stellar_baryon_fracs.csv", delimiter=",")
        Bud = np.genfromtxt(obs_dir+"Budzynski_stellar_masses.csv", delimiter=",")
        Ill = np.genfromtxt(obs_dir+"Illustris_stellar_fracs.csv", delimiter=",")
    else:
        chiu = np.genfromtxt(obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        chiu2 = np.genfromtxt(obs_dir+"Chiu_2016_baryon_fracs.csv", delimiter=",")
        gio = np.genfromtxt(obs_dir+"giodini2009_star_fracs.txt")
        lin = np.genfromtxt(obs_dir+"lin_mohr_table1.txt", usecols=range(0,19))
        van = np.genfromtxt(obs_dir+"van_der_burg_stellar_masses.csv", delimiter=",")
        #anders = np.genfromtxt(obs_dir+"Anderson2014_t1.csv", delimiter=",")

    salp2chab = 0.58 ## Factor to multiply stellar masses to convert from 
    ## studies using Salpeter IMF to a Chabrier 2003 IMF - factor from
    ## Madau 2014 "Cosmic SFH" (0.61) or Chiu 2016/Hilton 2013 (0.58)
    lin2chab = 0.76 ## Factor to convert Lin 2003 stellar masses to 
    ## equivalent if using a Chabrier IMF (0.76 from Chiu 2015)
    gonz2chab = 0.76 ## As above for Gonzalez 2014 paper

    plt.figure(figsize=(7,7))#width,height
    plt.ylabel(r"$\mathrm{M}_{\star}/\mathrm{M}_{500}$")
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(1e12, 2e15)
    plt.ylim(0.0, 0.07)
    plt.xscale('log')
    c = '0.7'
    ms = 7
    if z <= 0.15:
        if incl_ICL:
            plt.errorbar(gonz[:11,14]*0.702*1e14/h_scale, gonz[:11,29]*gonz2chab, xerr=[gonz[:11,15]*0.702/h_scale*1e14, gonz[:11,15]*0.702/h_scale*1e14], yerr=[gonz[:11,30]*gonz2chab, abs(gonz[:11,31]*gonz2chab)], c=c, mec='none', marker='o', ls='none', ms=ms*1.1, label="Gonzalez+ 2013")#(BCG + sats + ICL) # 0.05 < z < 0.12 # '#47d147'
            plt.errorbar(sand[:,3]*0.7*1e14/h_scale, sand[:,7]*gonz2chab, xerr=sand[:,4]*0.7/h_scale*1e14, yerr=sand[:,8]*gonz2chab, c=c, mec='none', marker='s', ls='none', ms=ms, label="Sanderson+ 2013")#(BCG + sats + ICL) # z=0.0571, 0.0796, 0.0943
            plt.errorbar(krav[:,5]*1e14*0.7/h_scale, (krav[:,11]+krav[:,14])/krav[:,5]/100, xerr=krav[:,6]*1e14*0.7/h_scale, yerr=(krav[:,11]+krav[:,14])/krav[:,5]/100*(((krav[:,12]**2+krav[:,15]**2)**0.5/(krav[:,11]+krav[:,14]))**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Kravtsov+ 2014")#(BCG + sats + ICL) #z < 0.1 # '#00cc99'
            #plt.errorbar(Bud[:,0]*0.71/h_scale, Bud[:,9], xerr=[Bud[:,1]*0.71/h_scale, Bud[:,2]*0.71/h_scale], yerr=[Bud[:,10], Bud[:,11]], c='k', mec='none', marker='o', ls='none', ms=ms, label="Budzynski+ 2013")#(BCG + sats + ICL) # z=0.15 - 0.4
            plt.errorbar(10**Ill[:,0]*0.704/h_scale, Ill[:,1], dash_capstyle='round', dashes=(4,6), lw=2.5, c='0.5', label="Illustris")
        else:
            mask = (chiu[:,3] < 0.15) ## redshift mask
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='s', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats)")#0.1 < z < 0.15 # '#ff9980'
            plt.errorbar(2.55e13*(lin[:,2]**1.58)*0.7/h_scale, lin[:,16]*0.01*lin2chab, yerr=[(lin[:,18])*0.01,(lin[:,17])*0.01*lin2chab], c=c, mec='none', marker='D', ls='none', ms=ms, label="Lin+ 2003 (BCG + sats)")# 0.016 < z < 0.09 # '#ff9cff'
    if z >= 0.0 and z < 1.0:## z = 0.1 - 1.0
        if not incl_ICL:
            plt.errorbar(gio[:,0]*0.72/h_scale, gio[:,3]*salp2chab, xerr=[(gio[:,0]-gio[:,1])*0.72/h_scale, (gio[:,2]-gio[:,0])*0.72], yerr=[(gio[:,3]-gio[:,4])*salp2chab,(gio[:,5]-gio[:,3])*salp2chab], c=c, mec='none', marker='^', ls='none', ms=ms, label=r"Giodini+ 2009")# (BCG + sats) $0.1 < z < 1.0$ # '#6ea3ff' # errors are the st dev
    if z >= 0.15 and z <=0.4:
        if incl_ICL:
            plt.errorbar(Bud[:,0]*0.71/h_scale, Bud[:,9], xerr=[Bud[:,1]*0.71/h_scale, Bud[:,2]*0.71/h_scale], yerr=[Bud[:,10], Bud[:,11]], c='k', mec='none', marker='o', ls='none', ms=ms, label="Budzynski+ 2013 (BCG + sats + ICL)")
    if z >= 0.1 and z <= 0.5:
        if not incl_ICL:
            mask = (chiu[:,3] < 0.5)
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats) 0.1 < z < 0.5")
    if z >= 0.5 and z <= 0.8:
        if not incl_ICL:
            mask = (chiu[:,3] > 0.5) & (chiu[:,3] < 0.8)
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats) 0.5 < z < 0.8")
    if z >= 0.8 and z <= 1.1:
        if not incl_ICL:
            mask = (chiu[:,3] > 0.8)
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats) 0.8 < z < 1.0")
    if z >= 0.6 and z <= 1.4:
        if not incl_ICL:
            mask = (chiu2[:,1] > 0.5)
            plt.errorbar(chiu2[mask,2]*1e14*0.683/h_scale, chiu2[mask,10]/chiu2[mask,2]/100, xerr=chiu2[mask,3]*1e14*0.683/h_scale, yerr=chiu2[mask,10]/chiu2[mask,2]/100*((chiu2[mask,11]/chiu2[mask,2])**2 + (chiu2[mask,3]/chiu2[mask,2])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Mohr, McDonald+ 2016 (BCG + sats) 0.6 < z < 1.4")
    if z > 0.8 and z < 1.4:
        if not incl_ICL:
            ## Convert M200 to M500 by factor 0.631 as in paper
            plt.errorbar(van[:,7]*1e14*0.7/h_scale*0.631, van[:,17]/(van[:,7]*0.631)/100, xerr=[van[:,8]*1e14*0.7/h_scale*0.631, abs(van[:,9])*1e14*0.7*0.631], yerr=van[:,17]/(van[:,7]*0.631)/100*((van[:,18]/van[:,17])**2+(van[:,8]/van[:,7])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="van der Burg+ 2014 (BCG + sats) 0.86 < z 1.34")

    ## For median profiles
    xmin, xmax = plt.gca().get_xlim()
    #xmin, xmax = [1e11,1e15]
    print "xmin, xmax =", xmin, xmax
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if ls is None:
        ls=['dashed', 'dotted', 'dashdot', 'solid']+['solid']*(len(simdirs)-4)
    if not for_paper:
        zcol = ["#c7673e", "#78a44f", "#6b87c8","#934fce","#E6AB02","#be558f","#47af8d"]
    ms = 6.5
    mew = 1.2
    lw = 3
    
    bias = 1.0 ## mass bias factor, 1-b (M_X = (1-b)*M500)

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None: label = simdir[8:-1]
        else: label = labels[j]
        with h5py.File(basedir+simdir+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
            ## datasets names are MstarTotal, Mstar_r500, Mstar_BCG, Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats, Mstar_sats_r500
            M500 = np.asarray(f['M500'][:,0])
            if incl_ICL:
                #MstarFrac = f['Mstar_r500'][:,0] / f['M500'][:,0] / bias ## total mass of stellar particles within r500
                MstarFrac = (f['Mstar_BCG_r500'][:,0] + f['Mstar_sats_r500'][:,0]) / M500 / bias ## total stellar mass of BCG within r500 plus satellites within r500
            else:
                MstarFrac = (f['Mstar_BCG_30kpc'][:,0] + f['Mstar_sats_r500'][:,0]) / M500 / bias ## stellar mass of BCG within 30kpc plus satellites within r500
            if plot_med:
                med, bin_edges, n = binned_statistic(M500 * bias, MstarFrac, statistic='mean', bins=bins)
                #plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[j], label=label, lw=lw, ls=ls[j], dash_capstyle='round')
                N, bin_edges, n = binned_statistic(M500 * bias, MstarFrac, statistic='count', bins=bins)
                idx = np.min(np.where((N > 0) & (N < 10))[0])
                print "idx =", idx
                plt.plot(x[:idx+1], med[:idx+1], c=col[j], label=label, lw=lw, ls='solid', dash_capstyle='round')
                plt.plot(x[idx:], med[idx:], c=col[j], lw=lw, ls='dashed', dash_capstyle='round')
                
            if j in scat_idx or not plot_med:
                if plot_med:
                    label=None ## label should be added above instead
                plt.plot(M500 / h_scale * bias, MstarFrac, marker='D', mew=mew, c=col[j], mfc='none', mec=col[j], ms=ms, ls='none', label=label)
        
        Nzooms = len(zooms)
        if len(zoomdirs)>0 and zoomdirs[j] is not "":
            if for_paper:
                zcol = [col[j]]*len(zooms)
            for zidx, zoom in enumerate(np.concatenate((zooms, zooms2))):
                if verbose:
                    print "Doing zoom", zoom
                if for_paper:
                    label = None
                    marker = 'D'
                else:
                    label = zoom.replace("_","\_")
                    marker = 'D'
                with h5py.File(basedir+zoomdirs[j]+zoom+"/"+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
                    if incl_ICL:
                        #MstarFrac = f['Mstar_r500'][:,0] / f['M500'][:,0] / bias ## total stellar mass within r500
                        MstarFrac = (f['Mstar_BCG_r500'][:,0] + f['Mstar_sats_r500'][:,0]) / f['M500'][:,0] / bias ## total stellar mass of BCG within r500 plus satellites within r500
                    else:
                        MstarFrac = (f['Mstar_BCG_30kpc'][:,0] + f['Mstar_sats_r500'][:,0]) / f['M500'][:,0] / bias ## stellar mass of BCG within 30kpc plus satellites within r500
                    if zidx < Nzooms:
                        plt.plot(f['M500'][0] / h_scale * bias, MstarFrac[0], marker=marker, mec=zcol[zidx], mfc='none', mew=mew, ms=ms, ls='none', label=label)
                    else:## from zooms2 list
                        plt.plot(f['M500'][0] / h_scale * bias, MstarFrac[0], marker=marker, mec='none', mfc=zcol[zidx - Nzooms], mew=0.5, ms=ms, ls='none', label=label)
        
    if for_paper:
        #plt.legend(loc='upper right', frameon=False, borderaxespad=1, numpoints=1, ncol=1, fontsize='smaller')
        fontsize = 19 #'medium'
        handles, labels = plt.gca().axes.get_legend_handles_labels()
        n = len(simdirs)
        leg1 = plt.legend(handles[:n], labels[:n], loc='lower left', frameon=False, borderaxespad=0.8, numpoints=1, fontsize=fontsize, ncol=1)
        plt.gca().add_artist(leg1)
        leg2 = plt.legend(handles[n:], labels[n:], loc='upper right', frameon=False, borderaxespad=1, numpoints=1, fontsize=fontsize, ncol=1)
        plt.gca().add_artist(leg2)

    else: plt.legend(loc='upper left', frameon=False, numpoints=1, ncol=2)
    if z>0.0: plt.title("z = {:.1f}".format(z))
    
    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.tight_layout(pad=0.45)
        plt.savefig(outdir+outfilename)#, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename

if __name__ == "__main__":
    main()