#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017
Plot L-T for different AGN feedback models
@author: nh444
"""
outdir = "/home/nh444/Documents/paper/"
basedir="/home/nh444/data/project1/L-T_relations/"
simdirs = ["L40_512_NoDutyRadioWeak",
           "L40_512_NoDutyRadioInt",
           "L40_512_DutyRadioWeak",
           "L40_512_MDRIntRadioEff",
           ]
labels = ["weak radio",
          "stronger radio",
          "quasar duty cycle",
          "fiducial",#"Combined",
          ]
filenames = ["L-T_data_apec_0.5-10_default.txt"]
outname = "M-T_models.pdf"
idxScat = range(len(simdirs))
idxMed = range(len(simdirs))

snapnum = 25
h = 0.679
zoomdirs = None#["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
zooms = [[]]#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]

figsize=(7,7)
col = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396'] ## orange-blue - combination of Viget and Hydrangea, slightly altered
#mfc = ['none']*len(simdirs)*len(filenames)
#mec = col
ls=['solid']*len(simdirs)*len(filenames)
markers = ['D', 's', 'o', '^', '>', 'v', '<']
ms=7
lw=3
mew=1.6
core_excised = False
binwidth = 0.1 ## dex for mass in Msun 
from scaling.plotting.plot_M_T import plot_M_T
plot_M_T(simdirs, snapnum, filenames, projected=True,
         frameon=True, core_excised=core_excised,
         labels=labels, zoomdirs=zoomdirs, zooms=zooms,
         h=h, basedir=basedir, outname=outname, outdir=outdir,
         figsize=figsize, idxScat=idxScat, idxMed=idxMed,
         col=col, markers=markers, ls=ls,
         ms=ms, lw=lw, mew=mew, binwidth=binwidth)
