#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 13:47:05 2017

@author: nh444
"""
import snap

h_scale = 0.679
snapnum = 25
outdir = "/home/nh444/Documents/paper/"
outdir = "/data/curie4/nh444/project1/L-T_relations/L40_512_LDRIntRadioEff_025/"

basedir = "/data/curie4/nh444/project1/boxes/L40_512_LDRIntRadioEff/"
indir = "/data/curie4/nh444/project1/L-T_relations/L40_512_LDRIntRadioEff_025/"
mpc_units = False
highres_only = False
group = 0
outsuffix = "_lowmass"
loc = 'best' ## legend location

#==============================================================================
# basedir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/c384_box_Arepo_new/"
# indir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/c384_box_Arepo_new_025/"
# mpc_units = True
# highres_only = True
# group = 0
# outsuffix = "_highmass"
# loc = 'lower left' ## legend location
#==============================================================================

suffixes = ["_default_Tvir4", "_Rasia2012_Tvir4"]#, "_Rasia_Tvir4"]
labels = ["Fiducial", "Rasia+12", "Rasia+12 renormalised"]

from scaling.xray_python.xray_profiles_old import compare_profiles_old

sim = snap.snapshot(basedir, snapnum, header_only=True, mpc_units=mpc_units)
h = sim.header.hubble
z = sim.header.redshift
omega_m = sim.header.omega_m
    
filebases = [indir+"profiles_snap"+str(snapnum)+"_grp"+str(group)+cur_suffix for cur_suffix in suffixes]

compare_profiles_old(filebases, h, z, omega_m, projected=False, toPlot=["temperature"], #toPlot=["density", "temperature", "entropy", "luminosity"],
                 xlims=[0.007, 2.0], loc=loc, #ylims=[0,8],
                 foutbase="xray_profile"+outsuffix, outdir=outdir, labels=labels, h_scale=h_scale, for_paper=True)
