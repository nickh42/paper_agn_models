from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/home/nh444/Documents/paper/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted_winds.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="density", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=8.0, M500_max=-1,## (1e13 Msun) Use negative for inf
             fout_suffix="REXCESS", filtered=True,
             rmin=0.007, rmax=2.0, ##-1 for infinity
             ylims=[2e-5, 0.1],
             plot_mean=False, plot_median=False,
             compare="Croston")
    
