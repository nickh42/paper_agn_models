from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/home/nh444/Documents/paper/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted_winds.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="pressure", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True,
             sort_by_M500=True,
             plot_residual=False,
             M500_min=3.15, M500_max=25,## (1e13 Msun) Use negative for inf
             fout_suffix="Sun", filtered=True, comp_unfilt=False,
             rmin=0.007, rmax=2.0, ##-1 for infinity
             ylims=[0.02, 200],
             plot_mean=False, plot_median=False,
             compare="Sun", scaled=True)