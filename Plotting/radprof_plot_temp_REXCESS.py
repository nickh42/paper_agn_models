from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/home/nh444/Documents/paper/"
#outdir = "/data/curie4/nh444/project1/profiles/"

#indirs = ["/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/"]
#filename = "snap_025_profiles_mass_weighted_none.hdf5"

filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted.hdf5"

radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="temperature", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             M500_min=9.0, M500_max=-1,## (1e13 Msun) Use negative for inf
             fout_suffix="REXCESS",
             rmin=0.007, rmax=2.0, ##-1 for infinity
             plot_mean=False, plot_median=False,
             compare="REXCESS")
    
