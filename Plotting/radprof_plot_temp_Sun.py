from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/"]

outdir = "/home/nh444/Documents/paper/"
#outdir = "/data/curie4/nh444/project1/profiles/"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_Tvir3_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_Rasia_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted.hdf5"
filename = "snap_025_profiles_box_Arepo_new_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted_Tvir4.0.hdf5"
filename = "snap_025_profiles_MDRInt_plus_zooms_default_mass_weighted.hdf5"

#indirs = ["/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/"]
#filename = "snap_025_profiles_mass_weighted_none.hdf5"

#==============================================================================
# outdir = "/data/curie4/nh444/project1/profiles/"
# indirs = ["/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEff_noBH/"]
# #indirs = ["/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEff/"]
# filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"
# 
#==============================================================================
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="temperature", plot_colour_scale=True, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=None,
             sort_by_M500=True,
             filtered=True,
             M500_min=3.15, M500_max=25,## (1e13 Msun) Use negative for inf
             fout_suffix="Sun", #title="Best AGN model",
             #fout_suffix="Sun_noBH", title="no BHs",
             rmin=0.007, rmax=2.0, ##-1 for infinity
             plot_mean=False, plot_median=False, medians_only=False,
             compare="Sun")