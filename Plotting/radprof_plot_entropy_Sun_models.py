from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak/",
            "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt/",
            "/data/curie4/nh444/project1/profiles/L40_512_DutyRadioWeak/",
            "/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/",
           ]
file_labels = ["Ref",
              "Stronger radio",
              "Quasar duty cycle",
              "Combined",
              ]

outdir = "/home/nh444/Documents/paper/"
filename = "snap_025_profiles_mass_weighted_Rasia_winds.hdf5"
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="entropy", plot_colour_scale=False, for_paper=True,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             M500_min=2.0, M500_max=-1,## (1e13 Msun) Use negative for inf
             fout_suffix="Sun_models", filtered=True, scat_ind=[],
             rmin=0.007, rmax=2.0, ##-1 for infinity
             leglabelsize=16,
             compare="Sun")
    
