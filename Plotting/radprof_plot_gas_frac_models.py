from profiles.radprof_plot import radprof_plot

indirs = ["/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioWeak/",
            "/data/curie4/nh444/project1/profiles/L40_512_NoDutyRadioInt/",
            "/data/curie4/nh444/project1/profiles/L40_512_DutyRadioWeak/",
            "/data/curie4/nh444/project1/profiles/L40_512_LDRIntRadioEffBHVel/",
           ]
file_labels = ["weak radio",
              "stronger radio",
              "quasar duty cycle",
              "fiducial",#"Combined",
              ]
#from palettable.cartocolors.sequential import Teal_4 as pallette
#col=pallette.hex_colors[-3:]+['#E7298A']
#col = ['#95CEB5', '#50BFD7', '#287AC1', '#192473'] ## blues
col = ['#FF9819', '#B94400', '#6395EC', '#6154BE'] ## Hydrangea orange-blue colour scheme
col = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396'] ## orange-blue - combination of Viget and Hydrangea, slightly altered

outdir = "/home/nh444/Documents/paper/"
filename = "snap_025_profiles_mass_weighted_Rasia_winds.hdf5"
filename = "snap_025_profiles_mass_weighted_default_Tvir4.0.hdf5"
radprof_plot([indir+filename for indir in indirs], outdir=outdir,
             plot_only="gasfrac", plot_colour_scale=False, for_paper=True, colours=col,
             add_prof_labels=False, prof_labels=None,
             add_file_labels=True, file_labels=file_labels,
             sort_by_M500=True,
             M500_min=2.0, M500_max=-1,## (1e13 Msun) Use negative for inf
             fout_suffix="_models", filtered=True, comp_unfilt=True,
             scat_ind=[],
             rmin=0.007, rmax=2.0, ##-1 for infinity
             ylims=[0.0, 0.2],
             compare="none")
    
