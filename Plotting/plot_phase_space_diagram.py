#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 17:50:33 2016
Plot phase space plot for a given group
@author: nh444
"""
import snap
import os
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np
from scipy.constants import k as kboltzmann
from scipy.constants import eV
keV = 1000.0*eV
UnitLength_in_cm = 3.085678e21 ## 1.0 kpc
UnitMass_in_g = 1.9884430e43 ## 1.0e10 solar masses
ag_hydrogen = 0.70683 # Anders & Grevesse solar mass fractions
ag_helium = 0.27431
ag_metals = 0.01886
h_scale = 0.679

outdir = "/home/nh444/Documents/paper/"
snapnum = 25

simdir = "/data/curie4/nh444/project1/boxes/"
simname = "L40_512_MDRIntRadioEff"#"L40_512_LDRIntRadioEff"
mpc_units = False
groupnums = [1]
xlims = [-29, -22] ## log10
ylims = [2, 8]
outname = "phase_space_diagram_lowmass.pdf"

simdir = "/data/curie4/nh444/project1/zoom_ins/MDRInt/"
simname = "c384_MDRInt"
mpc_units = True
groupnums = [0]
xlims = [-29, -22] ## log10
ylims = [2, 9]
outname = "phase_space_diagram_highmass.pdf"

#==============================================================================
# simdir = "/data/curie4/nh444/project1/zoom_ins/LDRIntRadioEffBHVel/"
# simname ="c160_box_Arepo_new"
# mpc_units = True
# groupnums = [0]
# xlims = [-29, -22] ## log10
# ylims = [2, 8]
# outname = "phase_space_diagram_lowmass2.pdf"
#==============================================================================

if not os.path.exists(outdir):
    os.mkdir(outdir)


plot_metallicity=False ## Colour by mass-weighted metallicity rather than counts
log = True ## log scales on both axes
inKelvin = True
if inKelvin:
  tempnorm = keV/kboltzmann
else:
  tempnorm = 1.0
Tvir_thresh = 4.0

sim = snap.snapshot(simdir+simname+"/", snapnum, mpc_units=mpc_units)
#h = sim.header.hubble
z = sim.header.redshift
rhonorm = UnitMass_in_g / h_scale / (UnitLength_in_cm / h_scale / (1+z))**3 ## physical g cm^-3
sim.load_gasrho()
sim.get_temp()
sim.load_gaspos()
sim.load_gasmass()
if plot_metallicity:
    sim.load_Zgas()

for groupnum in groupnums:  
    M500 = sim.cat.group_m_crit500[groupnum]
    R500 = sim.cat.group_r_crit500[groupnum] / (1+z)
    T200 = sim.cat.group_T_crit200[groupnum]
    print "M500 = {:.1f}e13".format(M500 / 1e3 / h_scale)
    
    radius = sim.cat.group_r_crit500[groupnum]
    partrads_sq = ((sim.cat.group_pos[groupnum] - sim.gaspos)**2).sum(axis=1)
    ind = np.where(partrads_sq < radius*radius)[0]
    #ind = np.intersect1d(ind, np.where(np.logical_and((sim.temp >= 3e4*8.617342e-8) | (sim.gasrho <= 500*0.044*2.77536627e-8), sim.gasrho < 0.00085483/(1+sim.header.redshift)**3))[0])
    rho = sim.gasrho[ind] * rhonorm
    temp = sim.temp[ind]*tempnorm
    mass = sim.gasmass[ind] * 1e10 / h_scale
    totMass = np.sum(mass)
    
    Tcut = 3e4*8.617342e-8 * tempnorm
    Rhocut = 0.000880623/(1+z)**3 * rhonorm ## old value was 0.00085483/(1+z)**3 * rhonorm
    print "Rhocut =", Rhocut

    gamma = 0.25 ## default is 0.25 from Rasia 2012
    #N_def = 3e6 * (1e-27)**(0.25 - gamma) ## scale normalisation with gamma to match normalisation of Rasia 2012 at 1e-27 g/cm^3
    rasia = lambda rho: tempnorm * 3e6 * (1e-27)**(0.25 - gamma) * (rho/(h_scale/0.72)**2)**gamma ## Rasia 2012 cut-off relation - gives temp in keV
    rasia2 = lambda rho: tempnorm * 3e6 * (1e-27)**(0.25 - gamma) * (M500 / 56091.5) * (910.6 / (R500 / (1+z))) * (rho/(h_scale/0.72)**2)**gamma ## Rasia 2012 cut-off relation with scaling - gives temp in keV
    #c = 4.0
    #N = min(N_def, N_def * c * (M500 / 56000) * (910 / (R500 / (1+z))))
    N_def = 1e7 * (1e-27)**(0.25 - gamma)
    N = N_def * (M500 / 59666.9) * (1008.8 / (R500 / (1+z))) ## scale relative to M500/R500 of c448
    rasia3 = lambda rho: tempnorm * N * (rho/(h_scale/0.6774)**2)**gamma ## normalisation is chosen for h=0.6774 so convert rho to same h

    
    print "Excluded mass (fraction):"
    cutInd = np.where((temp < Tcut) | (rho > Rhocut))[0]
    DefCutMass = np.sum(mass[cutInd])
    print "Default: {:.3e} ({:.5f})".format(DefCutMass, DefCutMass/totMass)
    cutInd = np.where(temp < rasia(rho))[0]
    rasiaMass = np.sum(mass[cutInd])
    cutInd = np.where(temp < rasia2(rho))[0]
    rasia2Mass = np.sum(mass[cutInd])
    print "Rasia scaled: {:.3e} ({:.5f})".format(rasia2Mass, rasia2Mass/totMass)
    cutInd = np.where(temp < rasia3(rho))[0]
    rasia3Mass = np.sum(mass[cutInd])

    fig, ax = plt.subplots(1,1, figsize=(7,6.6))
    cmap = plt.get_cmap('Blues')
    
    if log:
        x = np.log10(rho)
        y = np.log10(temp)
#        xmin = int(np.min(x))-1
#        xmax = int(np.max(x))
#        ymin = int(np.min(y))-1
#        ymax = int(np.max(y))+1
        xmin, xmax = xlims[0], xlims[1]
        ymin, ymax = ylims[0], ylims[1]
        xbins = np.linspace(xmin, xmax, num=100)
        ybins = np.linspace(ymin, ymax, num=100)
    else:
        x = rho
        y = temp
#        xmin = np.min(x)
#        xmax = np.max(x)
#        ymin = np.min(y)
#        ymax = np.max(y)
        xmin, xmax = 10**xlims[0], 10**xlims[1]
        ymin, ymax = 10**ylims[0], 10**ylims[1]
        xbins = np.logspace(int(np.log10(xmin))-1, int(np.log10(xmax))+1, num=100)
        ybins = np.logspace(int(np.log10(ymin))-1, int(np.log10(ymax))+1, num=100)
        ax.set_xscale('log')
        ax.set_yscale('log')
    
    if plot_metallicity:
        Z = sim.Zgas[ind] / (1-sim.Zgas[ind]) * (ag_hydrogen+ag_helium)/ag_metals
        #print "np.max(Z[ind]) =", np.max(Z)
        hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=Z*mass) ## Total Z*mass in each bin
        mass_hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=mass)
        hist[hist>0] = hist[hist>0] / mass_hist[hist>0] ## mass weighted Z
        if log:
            img = ax.imshow(hist.transpose(), extent=[xmin, xmax, ymin, ymax], origin="lower",
                            #norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                            cmap=cmap, interpolation='none')
        else:
            img = ax.pcolor(xedges, yedges, hist.transpose(),
                            #norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                            cmap=cmap)
            plt.axis([10**(int(np.log10(xmin))-1), 10**(int(np.log10(xmax))+1), 10**(int(np.log10(ymin))-1), 10**(int(np.log10(ymax))+1)])
    else:
        hist, xedges, yedges = np.histogram2d(x, y, bins=[xbins, ybins], weights=mass)
        img = ax.pcolor(xedges, yedges, hist.transpose(),
                        norm=LogNorm(vmin=np.min(hist[hist>0]), vmax=np.max(hist)),
                        cmap=cmap)
        if not log:
            plt.axis([10**(int(np.log10(xmin))-1), 10**(int(np.log10(xmax))+1), 10**(int(np.log10(ymin))-1), 10**(int(np.log10(ymax))+1)])
            #plt.axis([2e-29, 2e-23, 5e-4, 3])
        
    #ax.plot(np.log10(sim.gasrho[ind] * rhonorm), np.log10(sim.temp[ind]*tempnorm), linestyle="none", marker=",", alpha=0.1)
    if log:
        ax.set_xlim(xlims[0], xlims[1])
        ax.set_ylim(ylims[0], ylims[1])
    else:
        ax.set_xlim(10**xlims[0], 10**xlims[1])
        ax.set_ylim(10**ylims[0], 10**ylims[1])
    cbar = fig.colorbar(img)
    if plot_metallicity:
        cbar.set_label("Z [Z$_{\odot}$]", labelpad=10)
    else:
        cbar.set_label("Mass [M$_{\odot}$]", labelpad=10)
    c = '0.4'
    #ax.axhline(y=np.log10(2.5*T200), ls='dotted', c='0.5')
    xmin, xmax = ax.get_xlim()
    if log:
        ax.axhline(y=np.log10(Tcut), ls='dashed', c=c, label="Fiducial cuts")# f$_{\mathrm{gas}}$="+"{:.3f}".format(DefCutMass/totMass))
        ax.axvline(x=np.log10(Rhocut), ls='dashed', c=c)
        #ax.axvspan(np.log10(Rhocut), xlims[1], alpha=0.1, color='0.5')
        #ax.axhline(y=np.log10(Tvir_thresh*T200*tempnorm), ls='dashdot', c=c, label="Temperature threshold") # "{:.0f}".format(Tvir_thresh)+" T$_{200}$"
        #ax.plot([xmin, xmax], np.log10([rasia(10**xmin), rasia(10**xmax)]), ls='solid', label="Rasia+ 2012")
        ax.plot([xmin, xmax], np.log10([rasia2(10**xmin), rasia2(10**xmax)]), c=c, ls='solid', label="Rasia+ 2012 (scaled with $T_{500}$)")
        #ax.plot([xmin, xmax], np.log10([rasia3(10**xmin), rasia3(10**xmax)]), c=c, ls='solid', label="Rasia+ 2012 (rescaled)")# f$_{\mathrm{gas}}$="+"{:.3f}".format(rasia3Mass/totMass))
    else:
        ax.axhline(y=Tcut, ls='dashed', c=c)#, label="Cold cut")# f$_{\mathrm{gas}}$="+"{:.3f}".format(DefCutMass/totMass))
        ax.axvline(x=Rhocut, ls='dashed', c=c)
        ax.plot([xmin, xmax], [rasia(xmin), rasia(xmax)], ls='solid', label="Rasia+ 2012")
        #ax.plot([xmin, xmax], [rasia2(xmin), rasia2(xmax)], ls='solid', label="scaled f={:.3f}".format(rasia2Mass/totMass))
        ax.plot([xmin, xmax], [rasia3(xmin), rasia3(xmax)], c=c, ls='solid', label="Rasia+ 2012 (rescaled)")# f$_{\mathrm{gas}}$="+"{:.3f}".format(rasia3Mass/totMass))
        #plt.loglog(rho[cutInd], temp[cutInd], ls='none', marker=".")
        
    
    ax.legend(frameon=True, fontsize=16, borderpad=0.5, borderaxespad=1, loc='lower right')
    if log:
        ax.set_xlabel(r"log$_{10}$($\rho$ [g cm$^{-3}$])", labelpad=10)
        if inKelvin: ax.set_ylabel("log$_{10}$(T [K])", labelpad=10)
        else: ax.set_ylabel("log$_{10}$(T [keV])", labelpad=10)
    else:
        ax.set_xlabel("Gas Density [g cm$^{-3}$]")
        if inKelvin: ax.set_ylabel("Temperature [K]")
        else: ax.set_ylabel("Temperature [keV]")
    
    #print "Saving..."
    plt.tight_layout()
    plt.savefig(outdir+outname, bbox_inches='tight', dpi=300)
    print "Saved to", outdir+outname
