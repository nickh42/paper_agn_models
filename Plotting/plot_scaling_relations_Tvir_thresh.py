#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017

@author: nh444
"""

outdir = "/data/curie4/nh444/project1/L-T_relations/"
basedir="/home/nh444/data/project1/L-T_relations/"
simdirs = ["L40_512_LDRIntRadioEffBHVel"]
labels = [r"No $T_{200}$ threshold", r"$T < 3 T_{200}$"]
snapnum = 25
h = 0.679

figsize=(7,7)
from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["o", "D"]
ls=['solid', 'solid']
ms=6.5
lw=3
mew=1.2
binwidth = 0.2 ## dex for mass in Msun
idxScat = [0, 1]
idxMed = []

### Core-excised L-T
core_excised = True
binwidth = 0.1
outname = "L-T_relation_cex_Tvir_thresh.pdf"
filenames = ["L-T_data_apec_0.5-10_Rasia_1e7.txt", "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"]
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filenames, core_excised=core_excised, labels=labels, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

### Non-core-excised L-T
core_excised = False
binwidth = 0.1
filenames = ["L-T_data_apec_0.5-10_Rasia_1e7.txt", "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"]
outname = "L-T_relation_cin_Tvir_thresh.pdf"
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filenames, core_excised=core_excised, labels=labels, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

