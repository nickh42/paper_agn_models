#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017

@author: nh444
"""

import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
    
outdir = "/home/nh444/Documents/paper/" #"/data/curie4/nh444/project1/L-T_relations/"
basedir="/home/nh444/data/project1/L-T_relations/"

simdirs = ["L40_512_MDRIntRadioEff"]
zoomdir = "/data/curie4/nh444/project1/L-T_relations/MDRInt/"
zooms = ["c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]
filenames = ["L-T_data_apec_0.5-10_default.txt", "L-T_data_apec_0.5-10_nocuts.txt", "L-T_data_apec_0.5-10_Rasia2012.txt"]
labels = ["Fiducial cut", "No cuts", "Rasia+ 2012"] # (rescaled with T$_{500}$)
outname = "L-T_diff_cuts.pdf"

snapnum = 25
h = 0.679

figsize=(7,7)
#from palettable.colorbrewer.qualitative import Dark2_8 as pallette
#col=pallette.hex_colors
#col = ['#225EA8', '#ED8600', '#ED9E00', 'orange', '#95d058', '#E7298A', 'black', 'blue']
#col = ['#192473', '#79c6c6'] ## same as temp profile comparison - looks better but doesn't work with both
col = ['#24439b', '#B94400', '#ED7600']
mfc = ['none', 'none', 'none']
#mec = ['#225EA8', '#ED7600']#['#192473', '#79c6c6']
mec = col
markers = ["D", "D", "D", "o", 's']
ls=['solid', 'solid', 'solid']
ms=None#[8, 6]
lw=3
mew=1.8
binwidth = 0.2 ## dex for mass in Msun
connected = False
idxMed = []

binwidth = 0.1

temp_idx = 5 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 14 for mass-weighted projected, 15 for em-weighted projected
lum_idx = 25 ## 1 for 3D luminosity, 25 for projected

if col is None:
    from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    col=pallette.hex_colors
if mec is None:
    mec = col
if mfc is None:
    mfc = ['none']*len(simdirs)*len(filenames)
if markers is None:
    markers = ["D", "s", "o"]
if ms is None:
    ms = [6.5]*len(simdirs)*len(filenames)
if ls is None:
    ls=['dashdot', 'dotted', 'dashed', 'solid']

#fig, ax_L, ax_T = plt.subplots(1, 2, figsize=figsize)
fig, ax = plt.subplots(figsize=figsize)

for simidx, simdir in enumerate(simdirs):
    for fileidx, filename in enumerate(filenames):
        idx = fileidx + simidx*(len(filenames))
        data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
        if zoomdir is not None and len(zooms)>0:
            for zoom in zooms:
                zdata = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                data = np.vstack((data, zdata))
        if fileidx==0:
            ref = data ## reference dataset
        else:
            ax.plot(data[:,temp_idx]/ref[:,temp_idx], data[:,lum_idx]/ref[:,lum_idx], marker=markers[idx], mfc=mfc[idx], mec=mec[idx], ms=ms[idx], ls='none', mew=mew, label=labels[idx])

#==============================================================================
# if zoomdir is not None and len(zooms)>0:
#     for fileidx, filename in enumerate(filenames):
#         for zoom in zooms:
#             data = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#             plt.plot(data[temp_idx], data[lum_idx], marker=markers[fileidx], mfc=mfc[fileidx], mec=mec[fileidx], mew=mew, ms=ms[fileidx], ls='none')
# 
#==============================================================================
#plt.xlim(0.4, 10)
#plt.ylim(1e41,1e46)
#plt.xscale('log')
#plt.yscale('log')
#plt.ylabel("L$_{500}^{\mathrm{bol}}$ E(z)$^{-1}$ [erg s$^{-1}$]")#^{\mathrm{bol}}
#plt.xlabel(r"T$_{500}$ [keV]")

#from scaling.plotting.plotObsDat import add_legends
#add_legends(plt.gca(), simdirs, labels, nfiles=len(filenames), frameon=frameon)
## Custom legend with different font colours and no markers:
n = (len(simdirs) - sum(label is None for label in labels)) * len(filenames) ## no. of sims plotted
ax = plt.gca()
handles, labels = ax.axes.get_legend_handles_labels()
print "handles =", handles
leg1 = ax.legend(handles[:n], labels[:n], loc='lower right', markerscale=0, handlelength=0, handletextpad=0, frameon=False, borderpad=0.4, borderaxespad=1, numpoints=1, ncol=1)
for idx, text in enumerate(leg1.get_texts()): ##set text colour
    text.set_color(col[idx])
ax.add_artist(leg1)
ax.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)

plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

plt.tight_layout(pad=0.45)
#fig.savefig(outdir+outname)#, bbox_inches='tight')
#print "Saved to", outdir+outname