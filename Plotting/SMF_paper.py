#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  4 12:19:10 2017

@author: nh444
"""
import numpy as np

obs_dir = "/home/nh444/vault/ObsData/L-T/"
use_fof = True ## Use halo file for M_delta and R_delta
masstab = False
mpc_units = False
h_scale = 0.679
xlims, ylims = [9, 12.3], [-5, -1.5]

basedir = "/data/curie4/nh444/project1/boxes/"
#outdir = "/home/nh444/Documents/paper/"
outdir = "/data/curie4/nh444/project1/stars/"
dirnames = [#"L40_512_LDRIntRadioEffBHVel/",
            "L40_512_MDRIntRadioEff/",
           ]
labels = ["Fiducial"]

#####   Best box z=0   #####
from palettable.colorbrewer.sequential import YlGnBu_4 as palette
col = palette.hex_colors[1:]

from palettable.colorbrewer.sequential import YlGnBu_8_r as pallette
cmax = 0.6 ## increase to include lighter colours
cmap=pallette.mpl_colormap
colour_idx = np.linspace(cmax, 0, 3)
col = cmap(colour_idx)
lw = 3.5

suffix = "_diff_rad_z0" ## needs underscore
from stars.SMF import plot_SMF_comparison
plot_SMF_comparison(dirnames, 25, for_paper=True, bcgs_only=False, compare_apertures=True, use_nperbin=True, ratio=False, apercol=col, lw=lw, outdir=outdir, basedir=basedir, suffix=suffix, xlims=xlims, ylims=ylims, h_scale=h_scale)

#####   Best box redshift   #####
#xlims, ylims = [9, 12.2], [-5, -1.5]
from stars.stellar_mass import stellar_mass
snapnums = [25,20,15,10]

from palettable.colorbrewer.sequential import YlGnBu_8_r as pallette
cmax = 0.9 ## increase to include lighter colours
cmap=pallette.mpl_colormap
colour_idx = np.linspace(cmax, 0, len(snapnums))
#from palettable.colorbrewer.sequential import YlGnBu_5 as palette
#col = ['0.8']+palette.hex_colors[-3:]
lw = 3

for dirname in dirnames:
    mstar = stellar_mass(basedir, dirname, "/data/curie4/nh444/project1/stars/", snapnums, obs_dir, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=mpc_units)
    mstar.colours = ['0.8']+list(cmap(colour_idx))[1:]
    #mstar.colours = col
    mstar.plot_SMF(plot="all", for_paper=True, bcgs_only=False, bcgs_and_total=False,
                   log=True, verbose=True, lw=lw, xlims=xlims, ylims=ylims, outdir=outdir)

#####   Models comparison    #####
basedir = "/data/curie4/nh444/project1/boxes/"
dirnames = ["L40_512_NoDutyRadioWeak/",
            "L40_512_NoDutyRadioInt/",
            "L40_512_DutyRadioWeak/",
            "L40_512_MDRIntRadioEff/",
            #"L40_512_LDRIntRadioEff_noBH/",
           ]
labels = ["weak radio",
          "stronger radio",
          "quasar duty cycle",
          "fiducial",#"Combined",
          #"No BHs",
          ]
suffix = "_models_z0"
#xlims, ylims = [9, 12.5], [-5, -1.5]

#from palettable.cartocolors.sequential import Teal_4 as pallette
#col=pallette.hex_colors[-3:]+['#E7298A']
#col = ['#BBD8C0', '#50BFD7', '#287AC1', '#192473'] ## blues
#col = ['#95CEB5', '#50BFD7', '#287AC1', '#192473'] ## blues 2
#col = ['#85CBB8','#409ccc','#192473','#D0298A'] ## blues with pink
#col = ['#88CCEE', '#332288', '#999933', '#AA4499'] ## Paul Tol optimised for grey scale printing
#col = ['#4477AA', '#117733', '#DDCC77', '#CC6677'] ## Paul Tol 4-colour palette
#col = ['#3366AA', '#11AA99', '#EE3333', '#992288'] ## Paul Tol, 4 colours from screen-optimised palette
#col = ['#3366AA', '#66AA55', '#EE7722', '#992288'] ## Paul Tol, 4 different colours from screen-optimised palette
#col = ['#E68E34', '#7FB972', '#488BC2', '#781C81'] ## Paul Tol picked from rainbow scheme equidistantly
#col = ['#404096', '#57A3AD', '#DEA73A', '#D92120'] ## Paul blue + orange/red from rainbow scheme for 4 colours
#col = ['#77AADD', '#332288', '#44AA99', '#E7298A'] ## Paul Tol blues + my pink
#col = ['#117733', '#44AA99', '#88CCEE', '#332288'] ## Paul Tol bluegreen
#col = ['#FF9819', '#B94400', '#6395EC', '#6154BE'] ## Hydrangea orange-blue colour scheme
#col = ['#4477AA', '#117733', '#DDCC77', '#CC6677']
#from palettable.colorbrewer.qualitative import Paired_4 as pallette
#col=pallette.hex_colors
#from palettable.tableau import Tableau_20 as pallette
#col=pallette.hex_colors
#col = ["#1395BA", "#0D3C55", "#ECB93E", "#F16C20"]##viget blue-orange
#col = ["#ECB93E", "#F16C20", "#1395BA", "#0D3C55"]##viget orange-blue
#col = ["#c7673e","#78a44f","#6b87c8",'#E7298A',"#934fce","#e6ab02","#be558f","#47af8d"]
col = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396', 'grey'] ## orange-blue - combination of Viget and Hydrangea, slightly altered
lw = 3.5

#col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
#outdir = "/data/curie4/nh444/project1/stars/"

#==============================================================================
# from stars.stellar_mass import stellar_mass
# snapnums = [4, 5, 6, 8, 10, 15, 20, 25]
# snapnums = [25]#,20,15,10]
# for dirname in dirnames:
#     mstar = stellar_mass(basedir, dirname, "/data/curie4/nh444/project1/stars/", snapnums, obs_dir, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=mpc_units)
#     mstar.get_SMF(bcgs_only=False, calc_30kpc=True, verbose=True) ## Calculate SMF and save as txt file
#     #mstar.show_satellite_mass_distribution([7], cumulative=True, fractional=True)
# 
#==============================================================================
from stars.SMF import plot_SMF_comparison
plot_SMF_comparison(dirnames, 25, for_paper=True, use_nperbin=True, bcgs_only=False,
                    ratio=False,
                    #thirtykpc=True,
                    twicehalfrad=True,
                    compare_apertures=False, datadir="/data/curie4/nh444/project1/stars/",
                    labels=labels, col=col, lw=lw, outdir=outdir, suffix=suffix, basedir=basedir, xlims=xlims, ylims=ylims, h_scale=h_scale)


#==============================================================================
# ## thermal winds comparison ###
# basedir = "/data/curie4/nh444/project1/boxes/"
# dirnames = ["L40_512_DutyIntRadioWeak/",
#             "L40_512_DutyIntRadioWeak_Thermal/",
#            ]
# labels = [r"Cold winds",# ($U_{therm} = 0$)",
#           r"Warm winds",# ($U_{therm} = E_k$)",
#           ]
# suffix = "_winds_z0" ## needs underscore
# xlims, ylims = [7, 12.2], [-5, -1]
# col = ['#6395EC', "#F2BE40", '#4D4396', "#D9501D"]
# plot_SMF_comparison(dirnames, 25, ratio=False, comp_sims=False, for_paper=True, use_nperbin=True, bcgs_only=False,
#                     twicehalfrad=True, compare_apertures=False, datadir="/data/curie4/nh444/project1/stars/",
#                     labels=labels, col=col, outdir=outdir, suffix=suffix, basedir=basedir, xlims=xlims, ylims=ylims, h_scale=h_scale)
# 
# 
# ## softenings comparison ###
# basedir = "/data/curie4/nh444/project1/boxes/"
# dirnames = [#"L40_512_LDRIntRadioEff/",
#             #"L40_512_LDRIntRadioEff_SoftSmall/",
#             "L40_512_NoDutyRadioWeak/",
#             "L40_512_NoDutyRadioWeak_HalfThermal_FixEta/",
#            ]
# labels = [r"Fiducial softening length",
#           r"Small softening length",
#           ]
# suffix = "_soft_z0" ## needs underscore
# xlims, ylims = [7, 12.2], [-5, -1]
# col = ['#6395EC','#4D4396', "#D9501D"]
# plot_SMF_comparison(dirnames, 25, ratio=False, comp_sims=False, for_paper=True, use_nperbin=True, bcgs_only=False,
#                     twicehalfrad=True, compare_apertures=False, datadir="/data/curie4/nh444/project1/stars/",
#                     labels=labels, col=col, outdir=outdir, suffix=suffix, basedir=basedir, xlims=xlims, ylims=ylims, h_scale=h_scale)
# 
# 
#==============================================================================
