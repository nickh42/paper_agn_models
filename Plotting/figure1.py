"""
Loads a saved numpy array and saves as an image with a logarithmic colour scale
Each element of the numpy array describes one pixel.
"""

import matplotlib.pyplot as plt
import numpy as np
#import sim_python.cosmo as cosmo
from scipy.constants import e
from scipy.constants import k
from matplotlib.colors import hsv_to_rgb
from matplotlib.colors import rgb_to_hsv
from mpl_toolkits.axes_grid.inset_locator import (inset_axes, InsetPosition, mark_inset)
from matplotlib.ticker import (MultipleLocator, MaxNLocator)

indir = "/home/nh444/data/project1/maps/MDRInt/"
outdir = "/home/nh444/Documents/paper/"

name_suffixes = ["c448_MDRInt_higherres"] #c448_MDRInt_higherres
#name_suffixes = ["c512_MDRInt"]
#name_suffixes = ["c448_MDRInt_az0.0_ax0.0",
#                 "c448_MDRInt_az0.5_ax0.0",
#                 "c448_MDRInt_az1.0_ax0.0",
#                 "c448_MDRInt_az1.5_ax0.0",
#                 "c448_MDRInt_az0.0_ax0.5",
#                 ]

logtemp = True ## logarithmic scale on temperature
kelvin = True ## plot temp in Kelvin instead of keV
mintempmin = 1e5 ## minimum min. temperature in keV or K
circrad = []#[5.0] ## list of radii (r500) of circles drawn at centre, set empty list to disable
show_labels = False ## show axis and tick labels on main plot

col = 'white' ## colour of lines and edges
alpha = 1.0 ## alpha of connecting lines
lw = 1.8 ## line width of spines
lw_ins = 1.4 ## line width of lines joining insets
cbar_dist = 0.055 ## distance of colourbars from right-hand panels e.g. 0.02
cbar_width = "7%"

for name_suffix in name_suffixes:
    fig, ax = plt.subplots(figsize=(15,15)) ### needs to be square for the border to have the same thickness on all edges
    fontsize = 'x-small'
       
    ## Load density and temp maps
    cenx,ceny,cenz,sidelength,zthick,a_z,a_x,a_z2,t,z,h,M500,R500 = np.load(indir+"map_density_"+name_suffix+"_proj.npz")["mappars"]
    dens = np.load(indir+"map_density_"+name_suffix+"_proj.npz")["maparr"]
    npix = dens.shape[0]
    assert dens.shape[1] == npix, "density map not square."
    temp = np.load(indir+"map_temperature_"+name_suffix+"_proj.npz")["maparr"]
    assert temp.shape[0] == temp.shape[1] == npix, "temp map does not match density map dimensions "+str(npix)+"x"+str(npix)
    assert np.all(dens > 0.0) and np.all(temp > 0.0), "Cannot have zero values."
    ## Load xray flux map
    xray = np.load(indir+"map_xray_"+name_suffix+"_proj.npz")["maparr"]
    assert xray.shape[0] == xray.shape[1] == npix, "xray map does not match density map dimensions "+str(npix)+"x"+str(npix)
    ## Scale maps
    dens *= 1e10 / h / (sidelength / h / (1+z) / npix)**2 ## convert 1e10 Msun/h per pixel to Msun/kpc^2
    dens = np.log10(dens)
    if kelvin:
        temp *= 1000.0 * e / k
    if logtemp:
        temp = np.log10(temp)
    ## Convert X-ray flux per pixel to flux per kpc^2
    xray /= (sidelength / h / (1+z) / npix)**2
    ## Convert X-ray flux per pixel to flux per arcmin^2
    #cm = cosmo.cosmology(h, 0.3065) ## omega_m = 0.3065
    #kpc_arcsec = cm.arcsec_to_comov(ang=1, z=max(z, 0.001)) * 1000.0 ## arcsec per ckpc/h
    #xray /= (sidelength / kpc_arcsec / 60.0 / npix)**2.0 ## divide by pixel area in arcmin^2
    xray = np.log10(xray)
    ## Get min. and max. values
    densmax = np.max(dens)
    densmin = np.min(dens)
    print "densmax, densmin =", densmax, densmin
    tempmax = np.max(temp)
    tempmin = np.min(temp)
    if logtemp and tempmin < np.log10(mintempmin):
        tempmin = np.log10(mintempmin)
    elif not logtemp and tempmin < mintempmin:
        tempmin = mintempmin
    print "tempmax, tempmin =", tempmax, tempmin
    
    densnorm = plt.Normalize(vmin=densmin, vmax=densmax)
    tempnorm = plt.Normalize(vmin=tempmin, vmax=tempmax)
       
    #from palettable.matplotlib import Inferno_20 as pallette
    #from palettable.cmocean.sequential import Thermal_20 as pallette
    from palettable.cmocean.sequential import Deep_20_r as pallette
    #from palettable.cmocean.sequential import Dense_20_r as pallette
    cmap = pallette.mpl_colormap
    #cmap = plt.get_cmap('gnuplot2') ## matplotlib colormap
    ## convert the rgb values corresponding to this colormap to hsv
    hsv = rgb_to_hsv(cmap(tempnorm(temp))[:,:,:3])
    ## replace value (brightness) with normalised density
    hsv[:,:,2] = densnorm(dens) ## value (brightness)
    rgb = hsv_to_rgb(hsv)
       
    unitfac = 1.0 / 1000.0 / h / (1+z) ## to convert ckpc/h to pMpc
    L = 0.5*sidelength * unitfac
    print "L =", L, "Mpc"
    img = ax.imshow(rgb,
                    extent=[-L, L, -L, L], origin="lower")
    
    for crad in circrad:
        circle = plt.Circle((0, 0), crad * R500 * unitfac, fc='none', ec='white', alpha=0.5, lw=2, ls='dashed')
        ax.add_artist(circle)
        
    if show_labels:
        ax.set_xlabel("x [Mpc]")
        ax.set_ylabel("y [Mpc]")
    else:
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.setp(ax.get_xticklines(), visible=False)
        plt.setp(ax.get_yticklines(), visible=False)
    
    for i in ax.spines.itervalues():
        i.set_color(col)
        i.set_linewidth(lw)
           
    ### Galaxy zoom-in 1 (bottom right)
    ## Set coords of zoom-in -- if not square, need to alter the size set by InsetPosition below
    ## For a_z=0.0, a_x=0.0:
    #pos = np.array([6.7, 7.6, 1.15, 2.05]) ## x1, x2, y1, y2 [pMpc]
    pos = np.array([4.15, 5.55, -5.2, -3.8]) ## x1, x2, y1, y2 [pMpc]
    #pos = np.array([2.9, 3.3, 5.7, 6.1]) ## x1, x2, y1, y2 [pMpc]
    ## For a_z=1.5, a_x=0.0:
    #pos = np.array([2.1, 4.6, 3.0, 5.5]) ## x1, x2, y1, y2 [pMpc]
    #pos = np.array([5.4, 6.4, -3.6, -2.6]) ## x1, x2, y1, y2 [pMpc]
    
    pospix = np.int_((L+pos)/(2*L)*npix) ## convert to pixel units
    curmap = temp
    g1max = np.max(curmap[pospix[2]:pospix[3],pospix[0]:pospix[1]])
    g1min = np.min(curmap[pospix[2]:pospix[3],pospix[0]:pospix[1]])
    #if g1min < 5.0:
        #g1min = 5.0
    if g1min < tempmin:
        g1min = tempmin
    print "g1min, g1max =", g1min, g1max
    #from palettable.cmocean.sequential import Gray_20_r as curcmap
    #curcmap = curcmap.mpl_colormap
    gax1 = inset_axes(ax, width="30%", height="30%", loc=2) ## width and height are overwritten by InsetPosition
    img2 = gax1.imshow(curmap, norm=plt.Normalize(vmin=g1min, vmax=g1max), cmap=cmap,
               #rgb,
               extent=[-L, L, -L, L], origin="lower")
    gax1.set_xlim(pos[0], pos[1])
    gax1.set_ylim(pos[2], pos[3])    
    g1size = 1.0/3.0
    ip1 = InsetPosition(ax, [1.0, 0.0, g1size, g1size]) ## position, size
    gax1.set_axes_locator(ip1)
    ## Draw lines to zoomed region
    mark_inset(ax, gax1, loc1=2, loc2=3, fc="none", ec=col, alpha=alpha, lw=lw_ins)
    ## Create axes for colourbar
    cgax1 = inset_axes(gax1, width=cbar_width, height="98%",
                 loc=3,
                 bbox_to_anchor=(1.0+cbar_dist, 0.01, 1, 1),
                 bbox_transform=gax1.transAxes,
                 borderpad=0,
                 )
    cbar = fig.colorbar(img2, cax=cgax1, orientation='vertical')
    #cbar.set_label(r"$\mathrm{log}_{10}$($\rho$ [$M_{\odot} \mathrm{kpc}^{-2}$])", size='small')
    if kelvin:
        cbar.set_label("$\mathrm{log}_{10}$(T [K])", labelpad=13, size=fontsize)
    else:
        cbar.set_label("$\mathrm{log}_{10}$(T [keV])", labelpad=13, size=fontsize)
    cbar.locator = MaxNLocator(nbins=6)
    cbar.update_ticks()
    ## Remove ticks and tick labels
    plt.setp(gax1.get_xticklabels(), visible=False)
    plt.setp(gax1.get_yticklabels(), visible=False)
    plt.setp(gax1.get_xticklines(), visible=False)
    plt.setp(gax1.get_yticklines(), visible=False)
    for i in gax1.spines.itervalues():
        i.set_color(col)
        i.set_linewidth(lw)
        
    
    ## X-ray core zoom-in
    x = R500 * unitfac ## pMpc
    print "R500 =", R500 * unitfac, "pMpc"
    pos = np.array([-x, x, -x, x]) ## x1, x2, y1, y2 [pMpc]
    ## if this is not square, need to alter the size set by InsetPosition below
    pospix = np.int_((L+pos)/(2*L)*npix)
    xraymax = np.max(xray[pospix[2]:pospix[3],pospix[0]:pospix[1]])
    xraymin = np.min(xray[pospix[2]:pospix[3],pospix[0]:pospix[1]])
    print "xraymin, xraymax =", xraymin, xraymax
    ax3 = inset_axes(ax, width="30%", height="30%", loc=2) ## width and height are overwritten by InsetPosition
    img3 = ax3.imshow(xray, norm=plt.Normalize(vmin=xraymin, vmax=xraymax), cmap=plt.get_cmap('magma'),
                      extent=[-L, L, -L, L], origin="lower")
    ax3.set_xlim(pos[0], pos[1])
    ax3.set_ylim(pos[2], pos[3])
    size = 1.0/3.0 ## size of zoom as fraction of ax size
    ip2 = InsetPosition(ax, [1.0, 0.5-0.5*size, size, size]) ## position, size
    ax3.set_axes_locator(ip2)
    ## Draw lines to zoomed region
    pp, p1, p2 = mark_inset(ax, ax3, loc1=2, loc2=3, fc="none", ls='-',
                            ec=col, alpha=alpha, lw=lw_ins)
    ## Remove joining lines
    #p1.set_visible(False)
    #p2.set_visible(False)
    ## Create axes for colourbar
    cax3 = inset_axes(ax3, width=cbar_width, height="98%",  # height : 50%
                 loc=3,
                 bbox_to_anchor=(1.0+cbar_dist, 0.01, 1, 1),
                 bbox_transform=ax3.transAxes,
                 borderpad=0,
                 )
    cbar = fig.colorbar(img3, cax=cax3, orientation='vertical')
    cbar.set_label("$\mathrm{log}_{10}$($S_{X}$ [erg s$^{-1}$ cm$^{-2}$ kpc$^{-2}$])", size=fontsize)
    cbar.locator = MaxNLocator(nbins=5)
    cbar.update_ticks()
    ## Remove ticks and tick labels
    plt.setp(ax3.get_xticklabels(), visible=False)
    plt.setp(ax3.get_yticklabels(), visible=False)
    plt.setp(ax3.get_xticklines(), visible=False)
    plt.setp(ax3.get_yticklines(), visible=False)
    for i in ax3.spines.itervalues():
        i.set_color(col)
        i.set_linewidth(lw)
    
    ### Galaxy zoom-in 2 (top right)
    ## Set coords of zoom-in -- if not square, need to alter the size set by InsetPosition below
    ## For a_z=0.0, a_x=0.0:
    pos = np.array([6.7, 7.6, 1.15, 2.05]) ## x1, x2, y1, y2 [pMpc]
    pos = np.array([2.2, 3.35, -2.4, -1.25]) ## x1, x2, y1, y2 [pMpc]
    pos = np.array([2.7, 3.7, 5.55, 6.55]) ## x1, x2, y1, y2 [pMpc]
    pos = np.array([-4.2, -3.2, 3.9, 4.9]) ## x1, x2, y1, y2 [pMpc]
    #pos = np.array([-5.1, -4.4, 2.3, 3.0]) ## x1, x2, y1, y2 [pMpc]
    
    pospix = np.int_((L+pos)/(2*L)*npix)
    curmap = dens
    g2max = np.max(curmap[pospix[2]:pospix[3],pospix[0]:pospix[1]])
    g2min = np.min(curmap[pospix[2]:pospix[3],pospix[0]:pospix[1]])
    #if g2min < tempmin:
        #g2min = tempmin
    if g2min < 5.1:
        g2min = 5.1
    print "g2min, g2max =", g2min, g2max
    gax2 = inset_axes(ax, width="30%", height="30%", loc=2) ## width and height are overwritten by InsetPosition
    img2 = gax2.imshow(curmap, norm=plt.Normalize(vmin=g2min, vmax=g2max), cmap=cmap,
               #rgb,
               extent=[-L, L, -L, L], origin="lower")
    gax2.set_xlim(pos[0], pos[1])
    gax2.set_ylim(pos[2], pos[3])    
    g2size = 1.0/3.0
    ip1 = InsetPosition(ax, [1.0, 1-g2size, g2size, g2size]) ## position, size
    gax2.set_axes_locator(ip1)
    ## Draw lines to zoomed region
    mark_inset(ax, gax2, loc1=2, loc2=3, fc="none", ec=col, alpha=alpha, lw=lw_ins)
    ## Create axes for colourbar
    cgax2 = inset_axes(gax2, width=cbar_width, height="98%",  # height : 50%
                 loc=3,
                 bbox_to_anchor=(1.0+cbar_dist, 0.01, 1, 1),
                 bbox_transform=gax2.transAxes,
                 borderpad=0,
                 )
    cbar = fig.colorbar(img2, cax=cgax2, orientation='vertical')
#    if kelvin:
#        cbar.set_label("$\mathrm{log}_{10}$(T [K])", size='small')
#    else:
#        cbar.set_label("$\mathrm{log}_{10}$(T [keV])", size='small')
    cbar.set_label(r"$\mathrm{log}_{10}$($\rho$ [$M_{\odot} \mathrm{kpc}^{-2}$])", labelpad=8, size=fontsize)
    cbar.locator = MaxNLocator(nbins=6)
    cbar.update_ticks()
    ## Remove ticks and tick labels
    plt.setp(gax2.get_xticklabels(), visible=False)
    plt.setp(gax2.get_yticklabels(), visible=False)
    plt.setp(gax2.get_xticklines(), visible=False)
    plt.setp(gax2.get_yticklines(), visible=False)
    fig.subplots_adjust(right=0.6) ## shift everything to the left so we can see it all in fig.show()
    for i in gax2.spines.itervalues():
        i.set_color(col)
        i.set_linewidth(lw)
    

    ### Colourbar ###
    ## Create image for colourbar
    num = 100
    col_ind = np.linspace(0.0, 1.0, num=num) ## array from 0 to 1
    ind = np.repeat(np.stack((col_ind, col_ind)), [1,num-1], axis=0) ## matrix of 0 to 1
    hsv = rgb_to_hsv(cmap(ind)[:,:,:3]) ## convert matrix to rgb colourmap then convert to hsv
    hsv[:,:,2] = np.rot90(ind) ## set value (brightness) to array from 0 to 1
    rgb = hsv_to_rgb(hsv) ## convert back to rgb
    cax = plt.axes([0,0,1,1]) ## create axes for colourbar
    ## Set size and location of colourbar
    #cpos = [0.77, 0.07] ## position bottom right with axes labels left and bottom
    #cpos = [0.09, 0.08] ## position bottom left with axes labels left and bottom
    cpos = [0.04, 0.03] ## position bottom left with axes labels right and top
    ip = InsetPosition(ax, [cpos[0],cpos[1],0.20,0.20]) ## set position and size
    cax.set_axes_locator(ip)
    cax.imshow(rgb, extent=[tempmin, tempmax, densmin, densmax], aspect='auto')
    ## Set colorbar ticks, labels etc
    cax.set_ylabel(r"$\mathrm{log}_{10}$($\rho$ [$M_{\odot} \mathrm{kpc}^{-2}$])", fontsize=fontsize)
    if kelvin:
        cax.set_xlabel("$\mathrm{log}_{10}$(T [K])", fontsize=fontsize)
    else:
        cax.set_xlabel("$\mathrm{log}_{10}$(T [keV])", fontsize=fontsize)
    cax.xaxis.label.set_color('white')
    cax.yaxis.label.set_color('white')
    cax.xaxis.set_major_locator(MultipleLocator(1)) ## minor tick spacing
    cax.xaxis.set_minor_locator(MultipleLocator(0.2)) ## minor tick spacing
    cax.yaxis.set_minor_locator(MultipleLocator(0.2))
    cax.tick_params(axis='x', which='both', colors='white', labelsize=11)
    cax.tick_params(axis='y', which='both', colors='white', labelsize=11)
    [i.set_color('white') for i in cax.spines.itervalues()] ## change colour of spines
    ## Shift axes and tick labels to top and right
    cax.xaxis.set_tick_params(labeltop=True, labelbottom=False) ## move tick labels to the top
    cax.xaxis.set_label_position('top') ## Move label to the top
    cax.yaxis.set_tick_params(labelright=True, labelleft=False) ## move tick labels to the right
    cax.yaxis.set_label_position('right') ## Move label to the right
    
    
    ### Distance scale ###
    figpos = np.array([0.1, 0.9]) ## position of scale in fig coords
    pos = figpos * 2*L - L ## convert to axes units
    ## Plot line 1 pMpc in length
    ax.plot([pos[0], pos[0]+1.0], [pos[1], pos[1]], lw=2, c='white')
    ax.text(pos[0]+0.5, pos[1]+0.4, "1 Mpc", horizontalalignment='center',
            color='white', fontsize='small')
    
    ## Add border
    axpos = ax.get_position()
    print "axpos =", axpos
        
    #fig.subplots_adjust(right=0.6) ## shift everything to the left so we can see it all in fig.show()
    
    fig.savefig(outdir+"map_hsv_"+name_suffix+".pdf", bbox_inches='tight', dpi=300)
    #print "Saved to", outdir+"map_hsv_"+name_suffix+".pdf"
    
    axpos2 = ax.get_position()
    print "axpos3 =", axpos
    delta = axpos2.y0 - axpos.y0#0.1625 ## correction needed to axpos.y after fig.savefig does tight layout
    ew = 0.003 ## width of border
    rect = plt.Rectangle(#(axpos.x0, axpos.y0), axpos.x1-axpos.x0, axpos.y1-axpos.y0,
                         (axpos.x0-ew, axpos.y0+delta-ew), (axpos.x1-axpos.x0)*(1.0+size)+2*ew, axpos.y1-delta-(axpos.y0+delta)+2*ew,
                                  fill=True, color='k', zorder=0,
                                  transform=fig.transFigure, figure=fig)
    fig.patches.extend([rect])
    
    fig.savefig(outdir+"map_hsv_"+name_suffix+".pdf", bbox_inches='tight', dpi=300)
    print "Saved to", outdir+"map_hsv_"+name_suffix+".pdf"
