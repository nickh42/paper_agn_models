#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017

@author: nh444
"""
def main():
    outdir = "/home/nh444/Documents/paper/" #"/data/curie4/nh444/project1/L-T_relations/"
    basedir="/home/nh444/data/project1/L-T_relations/"
    
    #simdirs = ["L40_512_LDRIntRadioEffBHVel"]
    #zoomdir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"
    #zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]
    
    simdirs = ["L40_512_MDRIntRadioEff"]
    zoomdir = "/data/curie4/nh444/project1/L-T_relations/MDRInt/"
    zooms = ["c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]
    
    filenames = ["L-T_data_apec_0.5-10_Rasia.txt", "L-T_data_apec_0.5-10_nocuts.txt"]#, "L-T_data_apec_0.5-10_default.txt"]
    filenames = ["L-T_data_apec_0.5-10_Rasia_SF.txt", "L-T_data_apec_0.5-10_Rasia.txt", "L-T_data_apec_0.5-10_Rasia_SF_Tvir_thresh_4.0.txt"]
    labels = ["Rasia", "Rasia no SF", "Rasia T$<4T_{200}$"]
    filenames = ["L-T_data_apec_0.5-10_default.txt", "L-T_data_apec_0.5-10_Rasia2012.txt"]
    labels = ["Fiducial cut", "Rasia+ 2012"] # (rescaled with T$_{500}$)
    #filenames = ["L-T_data_apec_0.5-10_nocuts.txt", "L-T_data_apec_0.5-10_default.txt", "L-T_data_apec_0.5-10_Rasia2012.txt"]
    #labels = ["No cuts", "Fiducial cut", "Rasia+ 2012"] # (rescaled with T$_{500}$)
    outname = "L-T_cuts.pdf"

    snapnum = 25
    h = 0.679
    
    figsize=(7,7)
    #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    #col=pallette.hex_colors
    #col = ['#225EA8', '#ED8600', '#ED9E00', 'orange', '#95d058', '#E7298A', 'black', 'blue']
    #col = ['#192473', '#79c6c6'] ## same as temp profile comparison - looks better but doesn't work with both
    col = ['#ECAA38', '#24439b', '#ED7600']
    col = ['#24439b', '#ED7600']
    mfc = ['none', 'none', 'none']
    #mec = ['#225EA8', '#ED7600']#['#192473', '#79c6c6']
    mec = col
    markers = ["D", "D", "D", "o", 's']
    ls=['solid', 'solid', 'solid']
    ms=[9,5]#[11, 8, 5]
    lw=3
    mew=1.8
    binwidth = 0.2 ## dex for mass in Msun
    idxScat = [0, 1, 2]
    connected = False
    idxMed = []
    
    #==============================================================================
    # ### Core-excised L-T
    # core_excised = True
    # binwidth = 0.1
    # outname = "L-T_relation_cex_cuts.pdf"
    # filenames = ["L-T_data_apec_0.5-10_Rasia_1e7_LT0.15-1.0r500.txt", "L-T_data_apec_0.5-10_nocuts_LT0.15-1.0r500.txt"]
    # from scaling.plotting.plot_L_T import plot_L_T
    # plot_L_T(simdirs, snapnum, filenames, zoomdir=zoomdir, zooms=zooms, core_excised=core_excised, labels=labels, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
    # 
    #==============================================================================
    ### Non-core-excised L-T
    core_excised = False
    binwidth = 0.1
    
    plot_L_T(simdirs, snapnum, filenames, indArrow=[], zoomdir=zoomdir, zooms=zooms, core_excised=core_excised, labels=labels, frameon=True, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, connected=connected, col=col, mec=mec, mfc=mfc, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

def plot_L_T(simdirs, snapnum, filenames, core_excised=False, labels=None, frameon=False, zoomdir=None, zooms=[], h=0.679, basedir="/home/nh444/data/project1/L-T_relations/", outname="L-T_relation.pdf", outdir="/home/nh444/data/project1/L-T_relations/",
             figsize=(7,7), idxScat=[], idxMed=[], indArrow=[], connected=False, col=None, mec=None, mfc=None, markers=None, ls=None, ms=None, lw=2, mew=1.2, binwidth=0.1):
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker
    import numpy as np
    import scipy.stats as stats
    
    assert len(simdirs) > 0, "simdirs is an empty list"
    if not isinstance(filenames, (list, tuple)):## if not a list, convert to list
        filenames = [filenames]
    
    temp_idx = 5 ## temperature index - 5 for spectral projected, 22 for spectral 3D, 14 for mass-weighted projected, 15 for em-weighted projected
    lum_idx = 25 ## 1 for 3D luminosity, 25 for projected
    
    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if mec is None:
        mec = col
    if mfc is None:
        mfc = ['none']*len(simdirs)*len(filenames)
    if markers is None:
        markers = ["D", "s", "o"]
    if ms is None:
        ms = [6.5]*len(simdirs)*len(filenames)
    if ls is None:
        ls=['dashdot', 'dotted', 'dashed', 'solid']
    
    fig = plt.figure(figsize=figsize)
    #c = '0.7'
    from scaling.plotting.plotObsDat import L_T
    L_T(plt.gca(), core_excised=core_excised, h=h)
    
    ## Median profiles
    xmin, xmax = [np.log10(0.16), np.log10(1.7)]
    num = int((xmax - xmin) / binwidth)
    bins = np.logspace(xmin, xmax, num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    for simidx, simdir in enumerate(simdirs):
        for fileidx, filename in enumerate(filenames):
            idx = fileidx + simidx*(len(filenames))
            data = np.loadtxt(basedir+simdirs[simidx]+"_"+str(snapnum).zfill(3)+"/"+filename)
            label = labels[idx]
            if idx in idxMed:
                med, bin_edges, n = stats.binned_statistic(data[:,temp_idx], data[:,lum_idx], statistic='median', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[idx], ls=ls[idx], lw=lw, dash_capstyle='round', label=label)
                label = None ## don't want two legend entries
            if idx in idxScat:
                plt.plot(data[:,temp_idx], data[:,lum_idx], marker=markers[idx], mfc=mfc[idx], mec=mec[idx], ms=ms[idx], ls='none', mew=mew, label=label)
                if connected:
                    if fileidx == 0:
                        data1 = data
                    else:
                        for i in range(len(data[:,0])):
                            plt.gca().annotate("",
                            xy=(data1[i,temp_idx], data1[i,lum_idx]), xycoords='data',
                            xytext=(data[i,temp_idx], data[i,lum_idx]), textcoords='data',
                            arrowprops=dict(arrowstyle='-'))#fc=col[idx], ec=None, #arrowstyle='->',
                            #width=0.5, headwidth=6, headlength=5, shrink=0.07, fill=True))##width, headwidth, headlength are in pts
                            #connectionstyle="arc3"))
                if len(indArrow) > 0:
                    if fileidx == 0:
                        data1 = data
                    else:
                        for i in indArrow:
                            #plt.gca().arrow(data1[i,temp_idx], data1[i,lum_idx], data[i,temp_idx]-data1[i,temp_idx], data[i,lum_idx]-data1[i,lum_idx], width=1.0, zorder=4)
                            plt.gca().annotate("",
                            xy=(data1[i,temp_idx], data1[i,lum_idx]), xycoords='data',
                            xytext=(data[i,temp_idx], data[i,lum_idx]), textcoords='data',
                            arrowprops=dict(fc=col[idx], ec=None, #arrowstyle='->',
                            width=0.5, headwidth=6, headlength=5, shrink=0.07, fill=True))##width, headwidth, headlength are in pts
                            #connectionstyle="arc3"))

    if zoomdir is not None and len(zooms)>0:
        for fileidx, filename in enumerate(filenames):
            for zoom in zooms:
                data = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
                plt.plot(data[temp_idx], data[lum_idx], marker=markers[fileidx], mfc=mfc[fileidx], mec=mec[fileidx], mew=mew, ms=ms[fileidx], ls='none')
    
    plt.xlim(0.4, 10)
    plt.ylim(1e41,1e46)
    plt.xscale('log')
    plt.yscale('log')
    if core_excised:
        plt.ylabel("L$_{500, \mathrm{ce}}^{\mathrm{bol}}$ E(z)$^{-1}$ [erg s$^{-1}$]")#^{\mathrm{bol}}
        plt.xlabel(r"T$_{500, \mathrm{ce}}$ [keV]")
    else:
        plt.ylabel("L$_{500}^{\mathrm{bol}}$ E(z)$^{-1}$ [erg s$^{-1}$]")#^{\mathrm{bol}}
        plt.xlabel(r"T$_{500}$ [keV]")
    
    #from scaling.plotting.plotObsDat import add_legends
    #add_legends(plt.gca(), simdirs, labels, nfiles=len(filenames), frameon=frameon)
    ## Custom legend with different font colours and no markers:
    n = (len(simdirs) - sum(label is None for label in labels)) * len(filenames) ## no. of sims plotted
    ax = plt.gca()
    handles, labels = ax.axes.get_legend_handles_labels()
    print "handles =", handles
    leg1 = ax.legend(handles[:n], labels[:n], loc='lower right', markerscale=0, handlelength=0, handletextpad=0, frameon=False, borderpad=0.4, borderaxespad=1, numpoints=1, ncol=1)
    for idx, text in enumerate(leg1.get_texts()): ##set text colour
        text.set_color(col[idx])
    ax.add_artist(leg1)
    ax.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, ncol=1)


    plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outname)#, bbox_inches='tight')
    print "Saved to", outdir+outname

if __name__ == "__main__":
    main()