#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 12:28:38 2016

@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
from os import chdir
chdir('/home/nh444/data/project1/L-T_relations')
outdir = "/home/nh444/Documents/paper/"
outdir = "/data/curie4/nh444/project1/L-T_relations/"

#simdirs = ["L40_512_LDRIntRadioEffBHVel"]
#zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]
simdirs = ["L40_512_MDRIntRadioEff"]
zoomdir = "/home/nh444/data/project1/L-T_relations/MDRInt/"
zooms = ["c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]
labels = ["Hot gas", "Multiphase cut", "All gas"]
snapnum = 25
obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
outfilename="gas_mass_fractions.pdf"
filename = "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt"
filename = "L-T_data_apec_0.5-10_default.txt"
h = 0.679

fig = plt.figure(figsize=(7,7))#width,height

## For median profiles
xmin, xmax = [1e12, 4e13]
num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

#col = ['#E7298A', '#E7298A', '0.3']
#col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
from palettable.colorbrewer.sequential import YlGnBu_9_r as pallette
#col=pallette.hex_colors[2:]
col = pallette.mpl_colormap([0.17])
mec = col
mfc = ['none']*len(col) # ['#C6DAF4','#B4D0F4']
markers = ["D", "D", "s"]
ls = ['solid', 'dashed', 'dashed', 'dashdot']
ms = 7
lw=3
mew=1.6

### OBSERVATIONS ###
from scaling.plotting.plotObsDat import gas_frac
gas_frac(plt.gca(), h=h, ms=5, lwerr=1.2)

data1 = np.loadtxt(simdirs[0]+"_"+str(snapnum).zfill(3)+"/"+filename)
plt.plot(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], marker=markers[0], mfc=mfc[0], mec=mec[0], ms=ms, ls='none', mew=mew, zorder=5)
#median is plotted below
for zoom in zooms:
    zdata = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    data1 = np.vstack((data1, zdata))
    plt.plot(zdata[12] * 1e10 / h, zdata[13]/zdata[12], marker=markers[0], mfc=mec[0], mec=mec[0], mew=mew, ms=ms, ls='none', zorder=5)
med, bin_edges, n = stats.binned_statistic(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], statistic='mean', bins=bins)
plt.errorbar(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[0], zorder=5)#(APEC $T_{min}=0.01$ keV)
    
#==============================================================================
# ## default cuts
# filename = "L-T_data_apec_0.5-10_default.txt"
# data1 = np.loadtxt(simdirs[0]+"_"+str(snapnum).zfill(3)+"/"+filename)
# #plt.plot(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], marker=markers[1], alpha=0.5, mec='none', mfc=col[1], ms=ms, ls='none', mew=mew, zorder=4)
# #median is plotted below
# for zoom in zooms:
#     zdata = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
#     data1 = np.vstack((data1, zdata))
#     #plt.plot(zdata[12] * 1e10 / h, zdata[13]/zdata[12], marker=markers[1], alpha=0.5, mec='none', mfc=col[1], mew=mew, ms=ms, ls='none', zorder=4)
# med, bin_edges, n = stats.binned_statistic(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], statistic='mean', bins=bins)
# plt.errorbar(x[np.isfinite(med)], med[np.isfinite(med)], c=col[1], ls=ls[1], lw=lw, dash_capstyle='round', label=labels[1], zorder=4)
# 
#==============================================================================
## without cuts
filename = "L-T_data_apec_0.5-10_nocuts.txt"
data1 = np.loadtxt(simdirs[0]+"_"+str(snapnum).zfill(3)+"/"+filename)
#plt.plot(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], marker=markers[1], alpha=0.5, mec='none', mfc=col[0], ms=ms, ls='none', mew=mew, zorder=4)
#median is plotted below
for zoom in zooms:
    zdata = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
    data1 = np.vstack((data1, zdata))
    #plt.plot(zdata[12] * 1e10 / h, zdata[13]/zdata[12], marker=markers[1], alpha=0.5, mec='none', mfc=col[0], mew=mew, ms=ms, ls='none', zorder=4)
med, bin_edges, n = stats.binned_statistic(data1[:,12] * 1e10 / h, data1[:,13]/data1[:,12], statistic='mean', bins=bins)
plt.errorbar(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[2], lw=lw, dash_capstyle='round', label=labels[2], zorder=4)#dashes=(0.5,7)


plt.ylabel(r"M$_{\mathrm{gas,}500}/$M$_{500}$")#, fontsize=16)
plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")#, fontsize=16)
plt.xlim(xmin, 3e15)##2e15
plt.ylim(0.0, 0.23)
plt.xscale('log')
#plt.yscale('log')

#==============================================================================
# handles, labels = plt.gca().axes.get_legend_handles_labels()
# n = 0
# leg1 = plt.legend(handles[:n], labels[:n], loc='lower right', frameon=False, numpoints=1, fontsize='small', ncol=1)
# ax = plt.gca().add_artist(leg1)
# plt.legend(handles[n:], labels[n:], loc='upper left', frameon=False, borderaxespad=1, columnspacing=0.5, numpoints=1, fontsize='small', ncol=2)
# 
#==============================================================================
handles, labels = plt.gca().axes.get_legend_handles_labels()
n1 = 7
n2 = 2
leg1 = plt.legend(handles[:n1], labels[:n1], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize='small', ncol=1)
ax = plt.gca().add_artist(leg1)
leg2 = plt.legend(handles[n1:-n2], labels[n1:-n2], loc='upper right', frameon=False, borderaxespad=1, numpoints=1, fontsize='small', ncol=1)
ax = plt.gca().add_artist(leg2)
plt.legend(handles[-n2:], labels[-n2:], loc='lower right', frameon=False, numpoints=1, fontsize='small', ncol=1)

plt.tight_layout(pad=0.45)
plt.savefig(outdir+outfilename)#, bbox_inches='tight')
print "Plot saved to", outdir+outfilename
