#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 17:17:16 2017

@author: nh444
"""
def main():
    # General settings
    obs_dir = "/data/vault/nh444/ObsData/mass_fracs/"
    N=300
    delta = 500.0
    ref = "crit"
    use_fof = True ## Use halo file for M_delta and R_delta
    h_scale = 0.679
    masstab = False
              
    basedir = "/data/curie4/nh444/project1/boxes/"
    simdirs = [#"L40_512_LDRIntRadioEffBHVel/",
               "L40_512_MDRIntRadioEff/",
               ]
    labels = [None, "Combined"]
    col = ['#E7298A']
    from palettable.colorbrewer.sequential import YlGnBu_9_r as pallette
    #col=pallette.hex_colors[2:]
    col = pallette.mpl_colormap([0.17])
    ls = ['solid']
    col2 = '#7FCDBB' #'#0C2C84', '#225EA8', '#1D91C0', '#41B6C4', '#7FCDBB', '#C7E9B4', '#EDF8B1', '#FFFFD9'
       
    datadir = "/data/curie4/nh444/project1/stars/" ## where stellar masses are saved
    outdir = "/home/nh444/Documents/paper/"
    snapnums = [25]
    
    zoombasedir = "/home/nh444/data/project1/zoom_ins/"
    #zoomdirs = ["LDRIntRadioEffBHVel/"] ## corresponding to each simdir
    #zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]
    zoomdirs = ["MDRInt/"]
    zooms = ["c128_MDRInt", "c192_MDRInt", "c256_MDRInt",
             "c320_MDRInt", "c384_MDRInt",
             "c448_MDRInt"]

    
    #from stars.stellar_mass import stellar_mass
#==============================================================================
#     for simdir in simdirs:
#         mstar = stellar_mass(basedir, simdir, datadir, snapnums, obs_dir, N=N, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=False)
#         #mstar.get_mass_fracs(M500min=1e12)
#         mstar.get_stellar_masses(M500min=1e12, verbose=True)
#         #mstar.plot_mstarfrac_v_mass(for_paper=True) #requires get_mass_fracs
#         #mstar.show_satellite_mass_distribution(range(0,1), cumulative=True, fractional=True)
#==============================================================================
#==============================================================================
#     for zoomdir in zoomdirs:
#         if zoomdir is not "":
#             for zoom in zooms:
#                 mstar = stellar_mass(zoombasedir+zoomdir, zoom+"/", datadir+zoomdir, snapnums, obs_dir, N=1, delta=delta, ref=ref, h_scale=h_scale, use_fof=use_fof, masstab=masstab, mpc_units=True)
#                 #mstar.get_mass_fracs()
#                 mstar.get_stellar_masses(N=1, verbose=True)
#                 #mstar.show_satellite_mass_distribution(range(0,1), fractional=True, cumulative=True)
#==============================================================================
    #from stars.stellar_frac import plot_mstarfrac_v_mass_comparison_components
    snapnum, z = 25, 0.0
    plot_mstarfrac_v_mass_comparison_components(datadir, simdirs, zoomdirs, zooms, snapnum, z,
                                                incl_ICL=True, verbose=True, outfilename="stellar_mass_fractions_incl_ICL.pdf", outdir=outdir,
                                                scat_idx=0, plot_med=True, for_paper=True, obs_dir=obs_dir, labels=labels, col=col, col2=col2, ls=ls, h_scale=h_scale)

def plot_mstarfrac_v_mass_comparison_components(basedir, simdirs, zoomdirs, zooms, snapnum, redshift, obs_dir="/data/vault/nh444/ObsData/mass_fracs/", incl_ICL=True, outfilename="default", outdir="/data/curie4/nh444/project1/stars/", for_paper=False, scat_idx=-1, labels=None, col=None, col2='r', hlightzooms=True, ls=None, plot_med=True, h_scale=0.7, verbose=False):
    """
    Plots stellar mass fraction within r500 using the output of stellar_mass.get_stellar_masses()
    zooms2 is another list of zooms which are plotted with the same colours as zooms but with open rather than solid symbols
    """
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.stats import binned_statistic
    import h5py
    import os

    z = redshift
    fout_base = "stellar_masses_"
    if outfilename == "default":
        if for_paper: outfilename = "stellar_mass_fractions_paper.pdf"
        else: outfilename = "stellar_mass_fractions.pdf"
    basedir = os.path.normpath(basedir)+"/"

    if incl_ICL:
        gonz = np.genfromtxt(obs_dir+"Gonzalez_2013_stellar_gas_masses.csv", delimiter=",")
        krav = np.genfromtxt(obs_dir+"kravtsov_stellar_masses2.csv", delimiter=",")
        sand = np.genfromtxt(obs_dir+"Sanderson_2013_stellar_baryon_fracs.csv", delimiter=",")
        Bud = np.genfromtxt(obs_dir+"Budzynski_stellar_masses.csv", delimiter=",")
        Ill = np.genfromtxt(obs_dir+"Illustris_stellar_fracs.csv", delimiter=",")
    else:
        chiu = np.genfromtxt(obs_dir+"Suhada2012_Chiu2016_XMM-BCS.csv", delimiter=",")
        chiu2 = np.genfromtxt(obs_dir+"Chiu_2016_baryon_fracs.csv", delimiter=",")
        gio = np.genfromtxt(obs_dir+"giodini2009_star_fracs.txt")
        lin = np.genfromtxt(obs_dir+"lin_mohr_table1.txt", usecols=range(0,19))
        van = np.genfromtxt(obs_dir+"van_der_burg_stellar_masses.csv", delimiter=",")
        #anders = np.genfromtxt(obs_dir+"Anderson2014_t1.csv", delimiter=",")

    salp2chab = 0.58 ## Factor to multiply stellar masses to convert from 
    ## studies using Salpeter IMF to a Chabrier 2003 IMF - factor from
    ## Madau 2014 "Cosmic SFH" (0.61) or Chiu 2016/Hilton 2013 (0.58)
    lin2chab = 0.76 ## Factor to convert Lin 2003 stellar masses to 
    ## equivalent if using a Chabrier IMF (0.76 from Chiu 2015)
    gonz2chab = 0.76 ## As above for Gonzalez 2014 paper

    plt.figure(figsize=(7,7))#width,height
    #plt.ylabel(r"$\mathrm{M}_{\star}/\mathrm{M}_{500}$")
    plt.ylabel(r"$\mathrm{M}_{\mathrm{star,}500}/\mathrm{M}_{500}$")
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")
    plt.xlim(1e12, 2e15)
    plt.ylim(0.0, 0.08)
    for label in plt.gca().yaxis.get_ticklabels()[1::2]:
        label.set_visible(False) ## skip every other tick label
    plt.xscale('log')
    c = '0.7'
    ms = 7
    if z <= 0.15:
        if incl_ICL:
            plt.errorbar(gonz[:11,14]*0.702*1e14/h_scale, gonz[:11,29]*gonz2chab, xerr=[gonz[:11,15]*0.702/h_scale*1e14, gonz[:11,15]*0.702/h_scale*1e14], yerr=[gonz[:11,30]*gonz2chab, abs(gonz[:11,31]*gonz2chab)], c=c, mec='none', marker='o', ls='none', ms=ms*1.1, label="Gonzalez+ 2013")#(BCG + sats + ICL) # 0.05 < z < 0.12 # '#47d147'
            plt.errorbar(sand[:,3]*0.7*1e14/h_scale, sand[:,7]*gonz2chab, xerr=sand[:,4]*0.7/h_scale*1e14, yerr=sand[:,8]*gonz2chab, c=c, mec='none', marker='d', ls='none', ms=ms*1.2, label="Sanderson+ 2013")#(BCG + sats + ICL) # z=0.0571, 0.0796, 0.0943
            plt.errorbar(krav[:,5]*1e14*0.7/h_scale, (krav[:,11]+krav[:,14])/krav[:,5]/100, xerr=krav[:,6]*1e14*0.7/h_scale, yerr=(krav[:,11]+krav[:,14])/krav[:,5]/100*(((krav[:,12]**2+krav[:,15]**2)**0.5/(krav[:,11]+krav[:,14]))**2)**0.5, c=c, mec='none', marker='s', ls='none', ms=ms, label="Kravtsov+ 2014")#(BCG + sats + ICL) #z < 0.1 # '#00cc99'
            #plt.errorbar(Bud[:,0]*0.71/h_scale, Bud[:,9], xerr=[Bud[:,1]*0.71/h_scale, Bud[:,2]*0.71/h_scale], yerr=[Bud[:,10], Bud[:,11]], c='k', mec='none', marker='o', ls='none', ms=ms, label="Budzynski+ 2013")#(BCG + sats + ICL) # z=0.15 - 0.4
            plt.errorbar(10**Ill[:,0]*0.704/h_scale, Ill[:,1], dash_capstyle='round', dashes=(4,6), lw=2.5, c='0.5', label="Illustris")
        else:
            mask = (chiu[:,3] < 0.15) ## redshift mask
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='s', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats)")#0.1 < z < 0.15 # '#ff9980'
            plt.errorbar(2.55e13*(lin[:,2]**1.58)*0.7/h_scale, lin[:,16]*0.01*lin2chab, yerr=[(lin[:,18])*0.01,(lin[:,17])*0.01*lin2chab], c=c, mec='none', marker='D', ls='none', ms=ms, label="Lin+ 2003 (BCG + sats)")# 0.016 < z < 0.09 # '#ff9cff'
    if z >= 0.0 and z < 1.0:## z = 0.1 - 1.0
        if not incl_ICL:
            plt.errorbar(gio[:,0]*0.72/h_scale, gio[:,3]*salp2chab, xerr=[(gio[:,0]-gio[:,1])*0.72/h_scale, (gio[:,2]-gio[:,0])*0.72], yerr=[(gio[:,3]-gio[:,4])*salp2chab,(gio[:,5]-gio[:,3])*salp2chab], c=c, mec='none', marker='^', ls='none', ms=ms, label=r"Giodini+ 2009")# (BCG + sats) $0.1 < z < 1.0$ # '#6ea3ff' # errors are the st dev
    if z >= 0.15 and z <=0.4:
        if incl_ICL:
            plt.errorbar(Bud[:,0]*0.71/h_scale, Bud[:,9], xerr=[Bud[:,1]*0.71/h_scale, Bud[:,2]*0.71/h_scale], yerr=[Bud[:,10], Bud[:,11]], c='k', mec='none', marker='o', ls='none', ms=ms, label="Budzynski+ 2013 (BCG + sats + ICL)")
    if z >= 0.1 and z <= 0.5:
        if not incl_ICL:
            mask = (chiu[:,3] < 0.5)
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats) 0.1 < z < 0.5")
    if z >= 0.5 and z <= 0.8:
        if not incl_ICL:
            mask = (chiu[:,3] > 0.5) & (chiu[:,3] < 0.8)
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats) 0.5 < z < 0.8")
    if z >= 0.8 and z <= 1.1:
        if not incl_ICL:
            mask = (chiu[:,3] > 0.8)
            plt.errorbar(chiu[mask,21]*1e13*0.7/h_scale, chiu[mask,30]/chiu[mask,21]/10, xerr=chiu[mask,22]*1e13*0.7/h_scale, yerr=chiu[mask,30]/chiu[mask,21]/10*((chiu[mask,31]/chiu[mask,30])**2 + (chiu[mask,22]/chiu[mask,21])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Saro, Mohr+ 2016 (BCG + sats) 0.8 < z < 1.0")
    if z >= 0.6 and z <= 1.4:
        if not incl_ICL:
            mask = (chiu2[:,1] > 0.5)
            plt.errorbar(chiu2[mask,2]*1e14*0.683/h_scale, chiu2[mask,10]/chiu2[mask,2]/100, xerr=chiu2[mask,3]*1e14*0.683/h_scale, yerr=chiu2[mask,10]/chiu2[mask,2]/100*((chiu2[mask,11]/chiu2[mask,2])**2 + (chiu2[mask,3]/chiu2[mask,2])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="Chiu, Mohr, McDonald+ 2016 (BCG + sats) 0.6 < z < 1.4")
    if z > 0.8 and z < 1.4:
        if not incl_ICL:
            ## Convert M200 to M500 by factor 0.631 as in paper
            plt.errorbar(van[:,7]*1e14*0.7/h_scale*0.631, van[:,17]/(van[:,7]*0.631)/100, xerr=[van[:,8]*1e14*0.7/h_scale*0.631, abs(van[:,9])*1e14*0.7*0.631], yerr=van[:,17]/(van[:,7]*0.631)/100*((van[:,18]/van[:,17])**2+(van[:,8]/van[:,7])**2)**0.5, c=c, mec='none', marker='D', ls='none', ms=ms, label="van der Burg+ 2014 (BCG + sats) 0.86 < z 1.34")

    ## For median profiles
    #xmin, xmax = plt.gca().get_xlim()
    xmin, xmax = 3e11, 2e14
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)

    if col is None:
        from palettable.colorbrewer.qualitative import Dark2_8 as pallette
        col=pallette.hex_colors
    if ls is None:
        ls=['dashed', 'dotted', 'dashdot', 'solid']+['solid']*(len(simdirs)-4)
    ms = 6.5
    mew = 1.4
    lw = 3
    
    bias = 1.0 ## mass bias factor, 1-b (M_X = (1-b)*M500)

    for j, simdir in enumerate(simdirs):
        if verbose:
            print "Doing simdir", simdir
        simdir = os.path.normpath(simdir)+"/"
        if labels is None: label = simdir[8:-1]
        else: label = labels[j]
        with h5py.File(basedir+simdir+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
            ## datasets names are MstarTotal, Mstar_r500, Mstar_BCG, Mstar_BCG_r500, Mstar_BCG_30kpc, Mstar_BCG_rcut, Mstar_sats, Mstar_sats_r500
            #MstarFrac_tot = (f['Mstar_BCG_r500'][:,0] + f['Mstar_sats_r500'][:,0]) / f['M500'][:,0] / bias ## total stellar mass of BCG within r500 plus satellites within r500
            MstarFrac_tot  = f['Mstar_r500'][:,0] / f['M500'][:,0] / bias
            #MstarFrac_aper = (f['Mstar_BCG_30kpc'][:,0] + f['Mstar_sats_r500'][:,0]) / f['M500'][:,0] / bias ## stellar mass of BCG within 30kpc plus satellites within r500
            MstarFrac_aper = (f['Mstar_BCG_inrad'][:,0] + f['Mstar_sats_r500_inrad'][:,0]) / f['M500'][:,0] / bias ## stellar mass of BCG within twice stellar half-mass radius plus satellites within r500
            M500_all = f['M500'][:,0]
            MstarFrac_tot_all = MstarFrac_tot
            MstarFrac_aper_all = MstarFrac_aper
                        
        if zoomdirs[j] is not "":
            for zidx, zoom in enumerate(zooms):
                with h5py.File(basedir+zoomdirs[j]+zoom+"/"+fout_base+str(snapnum).zfill(3)+".hdf5") as f:
                    #MstarFrac_tot = (f['Mstar_BCG_r500'][:,0] + f['Mstar_sats_r500'][:,0]) / f['M500'][:,0] / bias ## total stellar mass of BCG within r500 plus satellites within r500
                    MstarFrac_tot  = f['Mstar_r500'][:,0] / f['M500'][:,0] / bias
                    #MstarFrac_aper = (f['Mstar_BCG_30kpc'][:,0] + f['Mstar_sats_r500'][:,0]) / f['M500'][:,0] / bias ## stellar mass of BCG within 30kpc plus satellites within r500
                    MstarFrac_aper = (f['Mstar_BCG_inrad'][:,0] + f['Mstar_sats_r500_inrad'][:,0]) / f['M500'][:,0] / bias
                    M500_all = np.concatenate((M500_all, f['M500'][0]))
                    MstarFrac_tot_all = np.append(MstarFrac_tot_all, MstarFrac_tot[0])
                    MstarFrac_aper_all = np.append(MstarFrac_aper_all, MstarFrac_aper[0])
                    if hlightzooms:
                        if incl_ICL:
                            plt.plot(f['M500'][0] / h_scale * bias, MstarFrac_tot[0], marker='D', mew=mew, c=col[j], mfc=col[j], mec=col[j], ms=ms, ls='none', label=label)
                        else:
                            plt.plot(f['M500'][0] / h_scale * bias, MstarFrac_aper[0], marker='D', mew=mew, c=col[j], mfc=col[j], mec=col[j], ms=ms, ls='none', label=label)
        alpha = 1.0
        ## plot points
        if j == scat_idx or scat_idx < 0 or not plot_med:
            if plot_med:
                label=None ## label should be used for median line instead
            if incl_ICL:
                plt.plot(M500_all / h_scale * bias, MstarFrac_tot_all, marker='D', mew=mew, c=col[j], mfc='none', mec=col[j], ms=ms, ls='none', label=label)
                #plt.plot(M500_all[M500_all>x[-1]] / h_scale * bias, MstarFrac_aper_all[M500_all>x[-1]], alpha=alpha, marker='D', mew=mew, mfc='none', mec=col2, ms=ms, ls='none', label=label)
            else:
                plt.plot(M500_all / h_scale * bias, MstarFrac_aper_all, marker='D', mew=mew, c=col[j], mfc='none', mec=col[j], ms=ms, ls='none', label=label)
        ## plot median line
        if plot_med:
            if incl_ICL:
                med, bin_edges, n = binned_statistic(M500_all / h_scale * bias, MstarFrac_tot_all, statistic='mean', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[j], label=label, lw=lw, ls=ls[j], dash_capstyle='round')
                #med, bin_edges, n = binned_statistic(M500_all / h_scale * bias, MstarFrac_aper_all, statistic='mean', bins=bins)
                #plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], alpha=alpha, c=col2, label=label, lw=lw, ls=ls[j], dash_capstyle='round')
            else:
                med, bin_edges, n = binned_statistic(M500_all / h_scale * bias, MstarFrac_aper_all, statistic='mean', bins=bins)
                plt.plot(x[np.isfinite(med)], med[np.isfinite(med)], c=col[j], label=label, lw=lw, ls=ls[j], dash_capstyle='round')
        
    if for_paper: plt.legend(loc='upper right', frameon=False, borderaxespad=1, numpoints=1, ncol=1, fontsize='smaller')
    else: plt.legend(loc='upper left', frameon=False, numpoints=1, ncol=2)
    if z>0.0: plt.title("z = {:.1f}".format(z))
    
    if not for_paper:
        plt.savefig(outdir+outfilename)
    else:
        plt.savefig(outdir+outfilename, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename

if __name__ == "__main__":
    main()