#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 16:42:06 2018

@author: nh444
"""

import snap

outdir = "/home/nh444/data/project1/maps/MDRInt/"

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
simname = "c448_MDRInt"
mpc_units = True
highres_only = False
group = 0

#==============================================================================
# basedir = "/data/curie4/nh444/project1/boxes/"
# simnames = ["L40_512_MDRIntRadioEff"]
# mpc_units = False
# highres_only = False
# group = 0
#==============================================================================

ngb = 16 ## factor that multiplies the cell volume and is cube-rooted for the smoothing length
sidelength = 20000.0 ## image sidelength in pkpc (no h)
npix = 20000
snapnum = 25
name_suffix="c448_MDRInt_higherres"
maps = ["density", "temperature", "xray"]
arot = [[0.0,0.0], [0.5,0.0], [1.0,0.0], [1.5,0.0], [0.0, 0.5]]

sim = snap.snapshot(basedir+simname, snapnum, mpc_units=mpc_units)
h = sim.header.hubble
z = sim.header.redshift
R500 = sim.cat.group_r_crit500[group]
sidelength *= h * (1+z)
zthick = sidelength / 4.0 ## integration distance, 0 is whole box

print "sidelength =", sidelength, "[ckpc/h]"
print "npix =", npix
print "R500 =", R500, "[ckpc/h]"

sim.get_gas_projection(sidelength=sidelength, group=group, zthick=zthick, npix=npix,
                       use_vol=True, filebase="map_", save_maps=maps,
                        phase_filter=False, highres_only=highres_only, ngb=ngb,
                        #a_z=1.5, a_x=0.0,
                        name_suffix=name_suffix, nthreads=32,
                        hdf5=False, angular=False, chandra_band=True, outdir=outdir)

#==============================================================================
# for a in arot:
#     print "a_z=", a[0], "a_x=", a[1]
#     sim.get_gas_projection(sidelength=sidelength, group=group, zthick=zthick, npix=npix,
#                            use_vol=True, filebase="map_", save_maps=maps,
#                             phase_filter=False, highres_only=highres_only, ngb=ngb,
#                             a_z=a[0], a_x=a[1],
#                             name_suffix=name_suffix+"_az"+str(a[0])+"_ax"+str(a[1]), nthreads=32,
#                             hdf5=False, angular=False, outdir=outdir)
#==============================================================================
