#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017

@author: nh444
"""

outdir = "/home/nh444/Documents/paper/"
#outdir="/home/nh444/data/project1/L-T_relations/"
basedir="/home/nh444/data/project1/L-T_relations/"

labels = [None, "Combined"]
snapnum = 25
h = 0.679
suffix = "Rasia"
suffix = "default"
#suffix = "Tvir_thresh_4.0"
#suffix = "default_Tvir_thresh_4.0"
projected = True

### old model - also need to change zoomdir for SZ below
#simdirs = ["L40_512_LDRIntRadioEffBHVel"]
#zoomdir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"
#zoomdirs = [zoomdir]
#zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]#, "c512_box_Arepo_new"]

### New model
simdirs = ["L40_512_MDRIntRadioEff"]
zoomdirs = ["/data/curie4/nh444/project1/L-T_relations/MDRInt/"]
zooms = [["c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]]#, "c512_MDRInt"]]

figsize=(7,7)
col = ['#E7298A']
#from palettable.colorbrewer.sequential import RdPu_9_r as pallette
#col=pallette.hex_colors[3:]
from palettable.colorbrewer.sequential import YlGnBu_9_r as pallette
#col=pallette.hex_colors[2:] ## col[0] = #225EA8
col = pallette.mpl_colormap([0.17])
import matplotlib.colors
print "Using colour", matplotlib.colors.rgb2hex(col[0])
#print "Using colour", col[0]
markers = ["D"]
ls=['solid']
ms=7
lw=3
mew=1.6
binwidth = 0.2 ## dex for mass in Msun
idxScat = [0]
idxMed = []
hlightzooms = True ## show zooms as filled symbols

filename = "L-T_data_apec_0.5-10_"+suffix+".txt"
outname = "L-M500_relation.pdf"
from scaling.plotting.plot_L_M500 import plot_L_M500
plot_L_M500(simdirs, snapnum, filename, projected=projected, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

filename = "L-T_data_apec_0.5-10_"+suffix+".txt"
outname = "L-Mgas_relation.pdf"
from scaling.plotting.plot_L_Mgas import plot_L_Mgas
plot_L_Mgas(simdirs, snapnum, filename, projected=projected, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

filename = "L-T_data_apec_0.5-10_L0.1-2.4_"+suffix+".txt"
outname = "Lsoft-M500_relation.pdf"
from scaling.plotting.plot_Lsoft_M500 import plot_Lsoft_M500
plot_Lsoft_M500(simdirs, snapnum, filename, projected=projected, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
outname = "Lsoft-Mgas_relation.pdf"
from scaling.plotting.plot_Lsoft_Mgas import plot_Lsoft_Mgas
plot_Lsoft_Mgas(simdirs, snapnum, filename, projected=projected, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

### Core-excised L-T
core_excised = True
binwidth = 0.1
outname = "L-T_relation_cex.pdf"
filename = "L-T_data_apec_0.5-10_LT0.15-1.0r500_"+suffix+".txt"
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filename, projected=projected, core_excised=core_excised, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

### Non-core-excised L-T
core_excised = False
binwidth = 0.1
filename = "L-T_data_apec_0.5-10_"+suffix+".txt"
#filename = "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_5Mpc.txt"
outname = "L-T_relation_cin.pdf"
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filename, projected=projected, core_excised=core_excised, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

### Non-core-excised M-T
## Lensing masses:
core_excised = False
binwidth = 0.1
outname = "M-T_relation_cin.pdf"
#filename = "L-T_data_apec_0.5-10_"+suffix+".txt"
#XXL_aper = False
filename = "L-T_data_apec_0.5-10_T300kpc_"+suffix+".txt"
XXL_aper = True
from scaling.plotting.plot_M_T import plot_M_T
plot_M_T(simdirs, snapnum, filename, plotlabel="weak lensing", projected=projected, core_excised=core_excised, XXL_aper=XXL_aper, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
## X-ray masses:
core_excised = False
binwidth = 0.1
outname = "M-T_relation_cin_xray.pdf"
filename = "L-T_data_apec_0.5-10_"+suffix+".txt"
XXL_aper = False
from scaling.plotting.plot_M_T import plot_M_T
plot_M_T(simdirs, snapnum, filename, plotlabel="X-ray hydrostatic", projected=projected, core_excised=core_excised, XXL_aper=XXL_aper, labels=labels, zoomdirs=zoomdirs, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, hlightzooms=hlightzooms, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

#==============================================================================
# #zoomdirs = ["LDRIntRadioEffBHVel/"]
# zoomdirs = ["MDRInt/"]
# outfilename = "SZ-M500_relation.pdf"
# from scaling.SZ_scaling import plot_SZ_M500
# suffix = ""
# plot_SZ_M500(simdirs, snapnum, scat_idx=[0], Wang=True, plot_med=False, for_paper=True,
#              cylindrical=False, plot_5r500=True, suffix=suffix,
#              outdir=outdir, outfilename=outfilename,
#              zoomdirs=zoomdirs, zooms=zooms,
#              labels=labels, h_scale=h, figsize=figsize, col=col, ls=ls,
#              mew=mew, ms=ms)
#==============================================================================



