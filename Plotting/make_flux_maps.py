import scaling.xray_python.xray as xray
import snap
import os
import matplotlib.pyplot as plt
import numpy as np

basedir = "/data/curie4/nh444/project1/boxes/"
dirnames = ["L40_512_MDRIntRadioEff"]#["L40_512_LDRIntRadioEff/"]
outdir = "/data/curie4/nh444/project1/maps/"
mpc_units=False
highres_only = False
groups = [1]
size = 2.0*0.605*1000.0*0.679 ## grp 1 0.6 Mpc
file_suffix = "0.6Mpc_proj_16ngb_05-10" ## appends to end of output map filenames (underscore added)

basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
dirnames = ["c384_MDRInt"]
mpc_units = True
highres_only = True
outdir = "/home/nh444/data/project1/maps/MDRInt/"
groups = [0]
size = 2.0*1.51*1000.0*0.679 ## c384 1.5 Mpc
file_suffix = "1.5Mpc_proj_16ngb_05-10" ## appends to end of output map filenames (underscore added)

#==============================================================================
# basedir = "/home/nh444/data/project1/zoom_ins/MDRInt/"
# dirnames = ["c256_MDRInt"]
# mpc_units = True
# highres_only = True
# outdir = "/home/nh444/data/project1/maps/MDRInt/"
# groups = [0]
# size = 2.0*0.8*1000.0*0.679 ## c384 1.5 Mpc
# file_suffix = "0.8Mpc_proj_16ngb_05-10" ## appends to end of output map filenames (underscore added)
#==============================================================================

snapnum = 25
maps = ["flux"]#, "density", "temperature"]
T_weight = "EM" ## temperature weighting "mass" or "EM"
npix = 2048#1024
ngb = 16 ## factor to multiply cell "radius" to approximate SPH with 'ngb' neighbours, default 64
nthreads = 16
a_z, a_x, a_z2 = 0, 0, 0 ## rotations in pi radians, default is all 0 (along z direction)
    
chandra_band = True ## use 0.5-7 keV band instead of bolometric
use_sim_metallicities = False
filter_type = "default" ## "default", "Rasia", "Rasia2012" or "none"
N=None ## normalisation for Rasia filter: default (=None) is 1e7, Rasia+ 2012 use 3e6
fluxtype = "total"
Tvir_thresh = -1
#file_suffix += "_Rasia2012_Tvir4.0"
#file_suffix += "_default_Tvir4.0"
#file_suffix += "_Tvir4.0"
file_suffix += "_default"
#file_suffix += "_Rasia2012"
#file_suffix += "_none"
save_as_fits = False ## save as fits file instead of .npz
CiC = False ## use cloud-in-cell rather than SPH kernel
show_plot = False
masstab = False

full_box = False ## map of entire box
    
for i, dirname in enumerate(dirnames):
    sim = snap.snapshot(basedir+dirname, snapnum, header_only=True, mpc_units=mpc_units)
    h = sim.header.hubble
    z = sim.header.redshift
    if z < 0.001: z = 0.001
    omega_m = sim.header.omega_m
    
    if chandra_band:
        src = xray.xray_source(z,h,omega_m, fluxdir="/data/vault/nh444/ObsData/flux_tables_apec_05-10keV/")
    else:
        src = xray.xray_source(z,h,omega_m)
    
    print "Loading sim data..."
    src.load_sim(basedir+dirname, snapnum, mpc_units=mpc_units, masstab=masstab)
    
    if not os.path.exists(outdir+dirname):
        os.mkdir(outdir+dirname)
        
    if full_box:
        groups=[-1]
    
    if "flux" in maps:
        for group in groups:
            print "\nDoing group "+str(group)+"..."
            if full_box:
                filebase = outdir+dirname+"/box_flux_proj"
                r500c=0.0
            else:
                filebase = outdir+dirname+"/xray_flux_map_grp"+str(group)+"_"+file_suffix
                r500c = src.sim.cat.group_r_crit500[group]
            src.get_flux_projection(sidelength=size,
                                    group=group,
                                    #zthick=size, ## leave out or =0 for whole box proj
                                    npix=npix, use_vol=True, ngb=ngb, CiC=CiC,
                                    a_z=a_z, a_x=a_x, a_z2=a_z2,
                                    filebase=filebase, save_as_fits=save_as_fits,
                                    filter_type=filter_type, N=N, fluxtype=fluxtype,
                                    use_sim_metallicities=use_sim_metallicities,
                                    nthreads=nthreads, Tvir_thresh=Tvir_thresh, highres_only=highres_only)
            if show_plot and not save_as_fits:## plot saved array
                fig = plt.figure()
                ax = fig.add_subplot(111)
                dat = np.load(filebase+".npz")
                maparr = dat["maparr"]
                cenx,ceny,cenz,sidelength,zthick,mya_z,mya_x,mya_z2,R500,M500 = dat["mappars"]
                img = ax.imshow(maparr.transpose(), cmap=plt.get_cmap('viridis'),
                                extent=[-0.5*sidelength/1000.0, 0.5*sidelength/1000.0, -0.5*sidelength/1000.0, +0.5*sidelength/1000.0],
                                origin="lower")
                ax.set_xlabel("x [Mpc/h]",size="x-large")
                ax.set_ylabel("y [Mpc/h]",size="x-large")
                cbar = fig.colorbar(img)
                cbar.set_label("X-ray flux", size="x-large")
                plt.show()
    if "density" in maps or "temperature" in maps:
        for group in groups:
            r500c = src.sim.cat.group_r_crit500[group]
            filebase = outdir+dirname+"/gas_map_grp"+str(group)+"_"+file_suffix
            src.get_gas_projection(sidelength=r500c*0.6, group=group, zthick=r500c*0.6,
                                   npix=npix, use_vol=True, ngb=ngb, T_weight=T_weight,
                                   a_z=a_z, a_x=a_x, a_z2=a_z2,
                                   maps=maps, filebase=filebase,
                                   nthreads=nthreads, filter_type=filter_type, N=N,
                                   save_as_fits=save_as_fits, plot=True,
                                   use_sim_metallicities=use_sim_metallicities,
                                   Tvir_thresh=Tvir_thresh, highres_only=highres_only)

    
    
