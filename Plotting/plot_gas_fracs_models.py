#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  2 12:28:38 2016

@author: nh444
"""
def main():
    import numpy as np
    import matplotlib.pyplot as plt
    from os import chdir
    chdir('/home/nh444/data/project1/L-T_relations')
    outdir = "/home/nh444/Documents/paper/"
    
    simdirs = ["L40_512_NoDutyRadioWeak",
               "L40_512_NoDutyRadioInt",
               "L40_512_DutyRadioWeak",
               "L40_512_MDRIntRadioEff",
               ]
    labels = ["weak radio",
              "stronger radio",
              "quasar duty cycle",
              "fiducial",#"Combined",
              ]
    snapnum = 25
    
    outfilename="gas_mass_fractions_models.pdf"
    #filename = "L-T_data_apec_0.5-2_Rasia_c4.txt"
    filename = "L-T_data_apec_0.5-10_Rasia_c4_Tvir_thresh_2.5.txt"
    #filename = "L-T_data_apec_0.5-10_none_Tvir_thresh_2.5.txt"
    filename = "L-T_data_apec_0.5-10_Rasia_1e7.txt"
    filename = "L-T_data_apec_0.5-10_default_Tvir_thresh_4.0.txt" ## default cuts
    filename = "L-T_data_apec_0.5-10_default.txt"
    h = 0.679
    
    fig = plt.figure(figsize=(7,7))#width,height
    
    ## For median profiles
    xmin, xmax = [1e12,1e15]
    num = int((np.log10(xmax) - np.log10(xmin/10.0)) / 0.2)
    bins = np.logspace(np.log10(xmin/10.0), np.log10(xmax), num=num)
    x = 10**((np.log10(bins[:-1]) + np.log10(bins[1:]))/2)
    
    #from palettable.colorbrewer.qualitative import Dark2_8 as pallette
    #col=pallette.hex_colors
    #from palettable.cartocolors.sequential import Teal_4 as pallette
    #col=pallette.hex_colors[-3:]+['#E7298A']
    #col = ['#95CEB5', '#50BFD7', '#287AC1', '#192473'] ## blues
    #col = ["#c7673e","#78a44f","#6b87c8","#934fce","#e6ab02","#be558f","#47af8d"]
    col = ['#FF9819', '#B94400', '#6395EC', '#6154BE'] ## Hydrangea orange-blue colour scheme
    col = ["#F2BE40", "#D9501D", '#6395EC', '#4D4396'] ## orange-blue - combination of Viget and Hydrangea, slightly altered
    markers = ["^", "s", "o", "D"]
    ls=['dashdot', 'dotted', 'dashed', 'solid']
    ls = ['solid']*4
    ms = 6.5
    lw=3
    mew=1.2
    
    ### OBSERVATIONS ###
    from scaling.plotting.plotObsDat import gas_frac
    gas_frac(plt.gca(), h=h, c='0.7')
    
    bias = 1.0 ## M_X / M_true = 1-b
    for i, simdir in enumerate(simdirs):
        data = np.loadtxt(simdirs[i]+"_"+str(snapnum).zfill(3)+"/"+filename)
        M500 = data[:,12] * 1e10 / h * bias
        #plt.errorbar(M500, data[:,13]/data[:,12], marker=markers[0], mec='none', mfc=col[0], ms=ms, ls='none', mew=mew, zorder=4)
        #med, bin_edges, n = stats.binned_statistic(M500, data[:,13]/data[:,12], statistic='median', bins=bins)
        #plt.errorbar(x[np.isfinite(med)], med[np.isfinite(med)], c=col[0], ls=ls[0], lw=lw, dash_capstyle='round', label=labels[0], zorder=5)
        med, idx = get_mean(M500, data[:,13]/data[:,12] / bias, bins)
        plt.errorbar(x[:idx+1], med[:idx+1], c=col[i], lw=lw, ls='solid', label=labels[i], zorder=5)
        plt.errorbar(x[idx:], med[idx:], c=col[i], lw=lw, ls='dashed', dash_capstyle='round', zorder=5)
        
    plt.ylabel(r"M$_{\mathrm{gas}}$/M$_{500}$")#, fontsize=16)
    plt.xlabel(r"M$_{500}$ [M$_{\odot}$]")#, fontsize=16)
    plt.xlim(1e12, 1e15)##2e15
    plt.ylim(0.0, 0.18)
    plt.xscale('log')
    #plt.yscale('log')
    
    #from scaling.plotting.plotObsDat import add_legends
    #add_legends(plt.gca(), simdirs, labels)
    
    handles, labels = plt.gca().axes.get_legend_handles_labels()
#==============================================================================
#     n1 = 5 ## top left, the rest go top right, or
#     n2 = 4 ## bottom right (sims)
#     leg1 = plt.legend(handles[:n1], labels[:n1], loc='upper left', frameon=False, borderaxespad=1, numpoints=1, fontsize='small', ncol=1)
#     plt.gca().add_artist(leg1)
#     leg2 = plt.legend(handles[n1:-n2], labels[n1:-n2], loc='upper right', frameon=False, borderaxespad=1, numpoints=1, fontsize='small', ncol=1)
#     plt.gca().add_artist(leg2)
#     plt.legend(handles[-n2:], labels[-n2:], loc='lower right', frameon=False, numpoints=1, fontsize='small', ncol=1)
#         
#==============================================================================
    plt.legend(handles[-5:], labels[-5:], loc='upper left', frameon=False, borderaxespad=1, fontsize=19, numpoints=1, ncol=1)
    
    ## Set axis labels to decimal format not scientific when using log scale
    #plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
    #plt.setp(plt.gca().get_xticklabels(), fontsize=16)
    #plt.setp(plt.gca().get_yticklabels(), fontsize=16)
    
    plt.tight_layout(pad=0.45)
    fig.savefig(outdir+outfilename)#, bbox_inches='tight')
    print "Plot saved to", outdir+outfilename
    
def get_mean(x, values, bins, NforIdx=10):
    """ returns median and index of first bin with fewer than NforIdx values"""
    from scipy.stats import binned_statistic
    import numpy as np
    
    med = binned_statistic(x, values, statistic='mean', bins=bins)[0]
    N = binned_statistic(x, values, statistic='count', bins=bins)[0]
    idx = np.min(np.where((N > 0) & (N < NforIdx))[0])
    return med, idx
    
if __name__ == "__main__":
    main()