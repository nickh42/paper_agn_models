#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 16:10:58 2017
Plot difference between spectroscopic and mass-weighted global temperature
as a function of mass 
@author: nh444
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import matplotlib.ticker as ticker


outdir = "/home/nh444/Documents/paper/"
outname = "temp_diff.pdf"
#outdir = "/data/curie4/nh444/project1/L-T_relations/L40_512_LDRIntRadioEff_025/"
#outname = "temp_diff_response_comp.pdf"

indir = "/data/curie4/nh444/project1/L-T_relations/"
#simname = "L40_512_LDRIntRadioEffBHVel"
#zoomdir=indir+"LDRIntRadioEffBHVel/"
#zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new",
#        "c448_box_Arepo_new", "c512_box_Arepo_new",
#        ]
simname = "L40_512_MDRIntRadioEff"
zoomdir=indir+"MDRInt/"
zooms = ["c128_MDRInt", "c192_MDRInt", "c256_MDRInt", "c320_MDRInt", "c384_MDRInt", "c448_MDRInt"]#, "c512_MDRInt"]
snapnums = [25]
h = 0.679

markers = ['D', 'D', 'o', '^', '>', 'v', '<']
ms=[8, 6]
mew=1.8
col = ['#E7298A']
col = ['#24439b','#225EA8', 'orange', 'green']
col = ['#24439b', '#ED7600'] ## blue, orange

filename = "L-T_data_apec_0.5-10_default.txt"
#filename = "L-T_data_apec_0.5-10_Rasia2012_Tvir_thresh_4.0.txt"

fig = plt.figure(figsize=(7,7))
M500min = 0.0#3e12
highlight = []#[5, 44, 100, 200] ## group numbers to highlight

# temp_mw, temp_emw, temp_bw, temp_ew, temp_mw_3D, temp_emw_3D, temp_bw_3D, temp_ew_3D, temp_3D
# 14,      15,       16,      17,      18,         19,          20,         21,         22,
spec_idx_proj = 5 ## 5 for projected, 22 for 3D 
mw_idx = 18 ## 14 for projected mw, 18 for 3D mw
spec_idx_3D = 22 ## 5 for projected, 22 for 3D 

for isnap, snapnum in enumerate(snapnums):
    data = np.loadtxt(indir+simname+"_"+str(snapnum).zfill(3)+"/"+filename)
    Tspec_proj = data[:,spec_idx_proj] 
    Tspec_3D = data[:,spec_idx_3D] 
    Tmw = data[:, mw_idx]
    M500 = data[:,12]*1e10/h # Msun
    #ind = np.where(M500 >= M500min)[0]
    ind_proj = np.where(Tspec_proj >= 0.2)[0]
    ind_3D = np.where(Tspec_3D >= 0.2)[0]
    y = Tmw
    plt.plot(Tspec_proj[ind_proj], y[ind_proj], ls='none', marker=markers[0], mfc='none', mec=col[0], mew=mew, ms=ms[0],
             label="2D")
    plt.plot(Tspec_3D[ind_3D], y[ind_3D], ls='none', marker=markers[1], mfc='none', mec=col[1], mew=mew, ms=ms[1],
             label="3D")
    highind = np.in1d(data[:,0], highlight)
    plt.plot(Tspec_proj[highind], y[highind], ls='none', marker=markers[0], mfc='none', mec='r', mew=mew, ms=ms[0])
    
    for zoom in zooms:
        if os.path.exists(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename):
            data = np.loadtxt(zoomdir+zoom+"_"+str(snapnum).zfill(3)+"/"+filename)
            Tspec_proj = data[spec_idx_proj] 
            Tspec_3D = data[spec_idx_3D] 
            Tmw_zoom = data[mw_idx]
            M500 = data[12]*1e10/h # Msun
            print zoom, Tspec_proj, Tspec_3D, Tmw_zoom, M500
            plt.plot(Tspec_proj, Tmw_zoom, ls='none', marker=markers[0], ms=ms[0], mfc='none', mec=col[0], mew=mew)
            plt.plot(Tspec_3D, Tmw_zoom, ls='none', marker=markers[1], ms=ms[1], mfc='none', mec=col[1], mew=mew)
                
#plt.axhline(y=0.0, ls='dashed', c='black')
plt.xlim(0.1, 10)
plt.ylim(0.1, 10)
plt.plot([0.1,10],[0.1,10], c='k')
plt.xscale('log')
plt.yscale('log')
#plt.xlim(xmin=M500min)
#plt.xlabel(r"M$_{500}$ (M$_{\odot}$)")
#plt.ylabel(r"$(\mathrm{T}_{\mathrm{spec}}-\mathrm{T}_{\mathrm{mw}})/\mathrm{T}_{\mathrm{mw}}$")
plt.xlabel(r"T$_{500}^{\mathrm{spec}}$ [keV]")
plt.ylabel(r"T$_{500}^{\mathrm{mw}}$ [keV]")
#plt.ylim(-0.5, 0.55)
#plt.xlim(xmin=8e12)
plt.gca().xaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))
plt.gca().yaxis.set_major_formatter(ticker.FuncFormatter(lambda y,pos: ('{{:.{:1d}f}}'.format(int(np.maximum(-np.log10(y),0)))).format(y)))

#plt.legend(frameon=False, numpoints=1, loc='best')
ax = plt.gca()
handles, labels = ax.axes.get_legend_handles_labels()
leg1 = ax.legend(handles, labels, loc='lower right', markerscale=0, handlelength=0, handletextpad=0, frameon=False, borderpad=0.4, borderaxespad=1, numpoints=1, ncol=1)
for idx, text in enumerate(leg1.get_texts()): ##set text colour
    text.set_color(col[idx])
ax.add_artist(leg1)
    
plt.tight_layout()
plt.savefig(outdir+outname, bbox_inches='tight')
print "Output to", outdir+outname
