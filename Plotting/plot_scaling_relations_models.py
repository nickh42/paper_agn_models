#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017

@author: nh444
"""

outdir = "/home/nh444/Documents/paper/"
basedir="/home/nh444/data/project1/L-T_relations/"
simdirs = ["L40_512_NoDutyRadioWeak_HalfThermal_FixEta",
           "L40_512_NoDutyRadioInt_HalfThermal_FixEta",
           "L40_512_LowDutyRadioWeak_HalfThermal_FixEta",
           "L40_512_LowDutyRadioWeak2EmTh0_01_HalfThermal_FixEta_BHChangeVel_RadEff",
           ]
labels = ["Ref",
          "Stronger radio",
          "Quasar duty cycle",
          "Combined"]
snapnum = 25
h = 0.679
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
zoomdir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"
zooms = []#["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new"]

figsize=(7,7)
from palettable.colorbrewer.qualitative import Dark2_8 as pallette
col=pallette.hex_colors
markers = ["D", "s", "^", "o"]
ls=['dashdot', 'dotted', 'dashed', 'solid']
ms=6.5
lw=3
mew=1.2
binwidth = 0.2 ## dex for mass in Msun
idxScat = [3]
idxMed = [0,1,2,3]

outname = "L-M500_relation_models.pdf"
from scaling.plotting.plot_L_M500 import plot_L_M500
plot_L_M500(simdirs, snapnum, filename, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

outname = "L-Mgas_relation_models.pdf"
from scaling.plotting.plot_L_Mgas import plot_L_Mgas
plot_L_Mgas(simdirs, snapnum, filename, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

filename = "L-T_data_apec_0.5-7_Rasia_c4_Tvir_thresh_2.5_L0.1-2.4.txt"
outname = "Lsoft-M500_relation_models.pdf"
from scaling.plotting.plot_Lsoft_M500 import plot_Lsoft_M500
plot_Lsoft_M500(simdirs, snapnum, filename, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

filename = "L-T_data_apec_0.5-7_Rasia_c4_Tvir_thresh_2.5_L0.1-2.4.txt"
outname = "Lsoft-Mgas_relation_models.pdf"
from scaling.plotting.plot_Lsoft_Mgas import plot_Lsoft_Mgas
plot_Lsoft_Mgas(simdirs, snapnum, filename, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

core_excised = True
binwidth = 0.1
outname = "L-T_relation_models_cex.pdf"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt"
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filename, core_excised=core_excised, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

core_excised = False
binwidth = 0.1
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
outname = "L-T_relation_models_cin.pdf"
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filename, core_excised=core_excised, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

core_excised = True
binwidth = 0.1
outname = "M-T_relation_models_cex.pdf"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_core_ex_L.txt"
from scaling.plotting.plot_M_T import plot_M_T
plot_M_T(simdirs, snapnum, filename, core_excised=core_excised, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

core_excised = False
binwidth = 0.1
outname = "M-T_relation_models_cin.pdf"
filename = "L-T_data_apec_0.5-10_Rasia_1e7_Tvir_thresh_3.0.txt"
from scaling.plotting.plot_M_T import plot_M_T
plot_M_T(simdirs, snapnum, filename, core_excised=core_excised, labels=labels, zoomdir=zoomdir, zooms=zooms, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
