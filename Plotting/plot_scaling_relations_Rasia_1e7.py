#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 15:23:49 2017

@author: nh444
"""

outdir = "/home/nh444/Documents/paper/" #"/data/curie4/nh444/project1/L-T_relations/"
basedir="/home/nh444/data/project1/L-T_relations/"
simdirs = ["L40_512_LDRIntRadioEffBHVel"]
labels = ["Cold cut", "All gas", "Multiphase cut"]
zoomdir = "/data/curie4/nh444/project1/L-T_relations/LDRIntRadioEffBHVel/"
zooms = ["c96_box_Arepo_new", "c128_box_Arepo_new", "c160_box_Arepo_new", "c256_box_Arepo_new", "c320_box_Arepo_new", "c384_box_Arepo_new", "c448_box_Arepo_new"]
snapnum = 25
h = 0.679

figsize=(7,7)
#from palettable.colorbrewer.qualitative import Dark2_8 as pallette
#col=pallette.hex_colors
col = ['#225EA8', '#ED9E00', 'orange', '#95d058', '#E7298A', 'black', 'blue']
#col = ['#192473', '#79c6c6'] ## same as temp profile comparison - looks better but doesn't work with both
markers = ["D", "o", 's']
ls=['solid', 'solid', 'solid']
ms=6.5
lw=3
mew=1.4
binwidth = 0.2 ## dex for mass in Msun
idxScat = [0, 1, 2]
idxMed = []

#==============================================================================
# ### Core-excised L-T
# core_excised = True
# binwidth = 0.1
# outname = "L-T_relation_cex_cuts.pdf"
# filenames = ["L-T_data_apec_0.5-10_Rasia_1e7_LT0.15-1.0r500.txt", "L-T_data_apec_0.5-10_nocuts_LT0.15-1.0r500.txt"]
# from scaling.plotting.plot_L_T import plot_L_T
# plot_L_T(simdirs, snapnum, filenames, zoomdir=zoomdir, zooms=zooms, core_excised=core_excised, labels=labels, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)
# 
#==============================================================================
### Non-core-excised L-T
core_excised = False
binwidth = 0.1
filenames = ["L-T_data_apec_0.5-10_Rasia.txt", "L-T_data_apec_0.5-10_nocuts.txt"]#, "L-T_data_apec_0.5-10_default.txt"]
filenames = ["L-T_data_apec_0.5-10_Rasia_SF.txt", "L-T_data_apec_0.5-10_Rasia.txt", "L-T_data_apec_0.5-10_Rasia_SF_Tvir_thresh_4.0.txt"]
labels = ["Rasia", "Rasia no SF", "Rasia T$<4T_{200}$"]
filenames = ["L-T_data_apec_0.5-10_default_Tvir_thresh_4.0_setE.txt", "L-T_data_apec_0.5-10_Rasia2012_Tvir_thresh_4.0.txt"]
labels = ["Fiducial cut", "Rasia+ 2012"] # (rescaled with T$_{500}$)

outname = "L-T_cuts.pdf"
from scaling.plotting.plot_L_T import plot_L_T
plot_L_T(simdirs, snapnum, filenames, indArrow=[], zoomdir=zoomdir, zooms=zooms, core_excised=core_excised, labels=labels, frameon=True, h=h, basedir=basedir, outname=outname, outdir=outdir, figsize=figsize, idxScat=idxScat, idxMed=idxMed, col=col, markers=markers, ls=ls, ms=ms, lw=lw, mew=mew, binwidth=binwidth)

